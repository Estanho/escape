#ifndef COMANDO_H_INCLUDED
#define COMANDO_H_INCLUDED

typedef std::string variavel;

typedef struct comando Comando;

typedef struct comando{
    enum com {
        ///I/O
        PRINT,

        ///Jumps
        JG,
        JGE,
        JL,
        JLE,
        JE,
        JNE,
        JA,
        JAE,
        JB,
        JBE,
        JMP,

        ///Alocação
        LABEL,
        ADDR,

        ///Funcional
        PUSH,
        POP,
        CALL,
        RET,

        ///Escrevimento
        MOV,
        MOVI,

        ///Atomic
        START_ATOMIC,
        END_ATOMIC,

        ///Processos
        SET_PROC,
        START_PROC,
        END_PROC,

        ///Lógico-aritméticos
        CMP,
        ADD,
        SUB,
        MUL,
        IMUL,
        DIV,
        IDIV,
        AND,
        OR,
        NOT,
        XOR
    } COM;

    union ARGS {
        bool booleano; ///DEPRECATED
        variavel* var;
        std::string* str;
        int inteiro;
        int imm;
        char caractere;
        Mutex* mutex; ///DEPRECATED
        std::queue<Comando*>* comandos;
    }* args;

} Comando;

#endif // COMANDO_H_INCLUDED
