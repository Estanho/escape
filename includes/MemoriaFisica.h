#ifndef MEMORIA_FISICA_H_INCLUDED
#define MEMORIA_FISICA_H_INCLUDED

#include <cstdlib>
#include <cstdio>
#include "Memoria.h"
#include "MemoriaVirtual.h"



#if CORRECAO_MEMORIA
    extern int tabs_correcao_memoria;
    extern unsigned _tamPagina_;
#endif 

class MemoriaFisica
{
	protected :
	char * memoria_array;
	const unsigned tamMemoria;
	
	~MemoriaFisica();
	MemoriaFisica( unsigned tamMemoria );
	
	public :

	unsigned access_4( unsigned adress ); //leitura na memoria fisica
	void access_4( unsigned adress, unsigned value );//escrita na memoria fisica 

	friend class Memoria;
};


#endif 