#ifndef MEMORIA_VIRTUAL_H_INCLUDED
#define MEMORIA_VIRTUAL_H_INCLUDED

#include "Memoria.h"
#include "MemoriaFisica.h"

class MemoriaFisica;
class Memoria;

class MemoriaVirtual
{
	protected :
	MemoriaFisica  * const  memFisica;
	Memoria  * const  memoria;
	const unsigned tamMemVirtual;
	const unsigned tamPagina;
	const unsigned tamMemReal;
	
	public :
	
	MemoriaVirtual(	Memoria * memoria );
	virtual ~MemoriaVirtual();
	
	virtual unsigned access_4( unsigned adress)    = 0; ///Leitura na memoria virtual 4 bytes
	virtual void access_4( unsigned adress, unsigned value )  = 0 ;///escrita na memoria virtual 4 bytes

	virtual bool newPages( unsigned& paginaVirtual, unsigned numeroPaginas )  = 0;
	/**
		Aloca numeroPaginas CONSECUTIVAS na memoria VIRTUAL, sem necessitar por elas na memoria física ou em disco até serem acessadas.
		paginaVirtual é uma referencia com valor qualquer passado  na função, mas que deve no retorno ter a paginaVirtual que você alocou para as paginas.
		REFERENCIAS ( simbolizadas pelo & ) não são copias, o valor alterado fica após o fim da função

		Caso você não tenha espaço na memoria, retorne falso e ignore paginaVirtual.
		Caso você consiga alocar as paginas retorne verdadeiro e bote em paginaVirtual a primeira pagina.
	*/
	virtual void deletePages( unsigned paginaVirtual , unsigned numeroPaginas )  = 0;
	virtual bool statusPage( unsigned& paginaFisica , unsigned paginaVirtual )  = 0;
	 
	friend class Memoria;
};

#endif 
