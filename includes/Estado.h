#ifndef ESTADO_H_INCLUDED
#define ESTADO_H_INCLUDED

enum Estado {
    PRONTO, // Estado onde o processo esta pronto para ser executado
    ESPERA,  // Esta executando E/S de dados
    FINALIZADO
};

#endif // ESTADO_H_INCLUDED
