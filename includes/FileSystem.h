#ifndef  FILESYSTEM_H_INCLUDED
#define FILESYSTEM_H_INCLUDED

#define caminhoBase "DynamicFiles/"
#include <cstdio>
#include "TOOLS.h"
#include <cstdlib>
#include <map>
#include <iostream>

using namespace std;

struct File
{
  FILE* c_file;
  bool temp;
  //...
};


typedef map< unsigned , File >::iterator itMapFile;

class FileSystem 
{
  unsigned proxDesc;
  string proxNomeTempFile;
  map<  unsigned , File > arquivosAbertos;
  
public :
  FileSystem();
  
  unsigned newTempFile();
  // versão futura // unsigned openFile( const char* const file, modos );
  void closeFile( unsigned desc ) ;
  FILE* c_file( unsigned desc ) ;
  
  void rewind( unsigned desc ) ;
  size_t fwrite( void* vetor, size_t bytes, unsigned desc ) ;
  size_t fread( void * vetor, size_t bytes , unsigned desc ) ;
  
  ~FileSystem();
};

//futuro // class ProcFileSystem

#endif
