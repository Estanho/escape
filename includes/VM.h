#ifndef VM_H_INCLUDED
#define VM_H_INCLUDED
#include "includes.h"
#include "../GUI/gui.h"

#define ESCAPE_START MainWindow* Janela::ProcessWindow; \
                     MainWindow* Janela::MemoryWindow;  \
                     pthread_mutex_t Janela::OUTPUT;    \
                     std::queue<std::string> Janela::strptrs;
                     
#define MAIN_BEGIN   QApplication a(argc, argv); \
                     MainWindow w;
#define ESCAPE_RUN   Janela::p_Instance()->show(); \
                     a.exec();

typedef std::string nome_processo;

class VM {
private:
    std::map<nome_processo, PID> mapaRegistro;
    std::map<PID, Processo*> mapaProcesso;
    std::map<variavel, DATA> data;
    std::map<num_buffer, Buffer*> mapaBuffer; //Sim, eu gosto de maps
    std::queue<stdoutChar> stdOut;

    bool atomic;

    unsigned int nProc;
    unsigned int nProcAtivos;

    Escalonador* escalonador;
    Processo* processoAtivo;
    Memoria * memoria;

    bool rodando;

public:
    FileSystem fileSystem;

    VM(
    );

    ~VM(
    );
    
    bool set_Escalonador(
    	Escalonador*
    );
 
    Memoria * get_Memoria();

    PID criaProcesso(
        std::string
    );
    PID criaProcesso(
    );

    Processo* getProcesso(
        PID
    );

    void registrar(
        nome_processo,
        PID
    );

    void iniciar(
    );

    DATA* getvar(
        variavel
    );

    /** COMANDOS **/

private: //TEM QUE SER PRIVATE PORRA

    void subcm_imprime( 
        variavel& ,
        unsigned 
    );
    void cm_mov( 
        const variavel&,
        const variavel& 
    );
    void cm_movi( 
        const variavel&,
        const int 
    );
    
    void cm_imprime(
        string
    );

    void cm_imprime_var(
        variavel
    );

    void cm_branchFun(
        int
    );

    void cm_branchRet(
    );

    void cm_branchf( //Branch if false
        variavel,
        int
    );

    void cm_brancht( //Branch if true
        variavel,
        int
    );

    void cm_branch(
        int
    );
    void cm_jg(
        int
    );
    void cm_jge(
        int
    );
    void cm_jl(
        int
    );
    void cm_jle(
        int
    );
    void cm_je(
        int
    );
    void cm_jne(
        int
    );

    void cm_alloc_var(
        variavel, //Nome da variavel
        bool
    );
    void cm_write_var(
        variavel,
        int
    );
    void cm_write_var(
        variavel,
        variavel
    );

    void cm_set_proc(
        std::queue<Comando*>,
        nome_processo,
        variavel
    );

    void cm_start_atomic(
    );
    void cm_end_atomic(
    );
    void cm_end_proc(
    );


    void cm_add(
        variavel,
        variavel
    );

    void cm_sub(
        variavel,
        variavel
    );

    void cm_mul(
        variavel
    );

    void cm_mulc(
        variavel,
        variavel,
        int
    );

    void cm_div(
        variavel,
        variavel
    );

    void cm_divc(
        variavel,
        variavel,
        int
    );

    void cm_cdiv(
        variavel,
        variavel,
        int
    );

    void cm_cmp(
        const variavel&,
        const variavel&
    );

    void cm_and(
        variavel,
        variavel,
        variavel
    );

    void cm_andc(
        variavel,
        variavel,
        int
    );

    void cm_or(
        variavel,
        variavel,
        variavel
    );

    void cm_orc(
        variavel,
        variavel,
        int
    );

    void cm_not(
        variavel,
        variavel
    );

    void cm_notc(
        variavel,
        int
    );

    void cm_xor(
        variavel,
        variavel,
        variavel
    );

    void cm_xorc(
        variavel,
        variavel,
        int
    );

    void cm_andl(
        variavel,
        variavel,
        variavel
    );

    void cm_andlc(
        variavel,
        variavel,
        int
    );

    void cm_orl(
        variavel,
        variavel,
        variavel
    );

    void cm_orlc(
        variavel,
        variavel,
        int
    );

    void cm_notl(
        variavel,
        variavel
    );

    void cm_notlc(
        variavel,
        int
    );

    void cm_xorl(
        variavel,
        variavel,
        variavel
    );

    void cm_xorlc(
        variavel,
        variavel,
        int
    );

    void cm_gt(
        variavel,
        variavel,
        variavel
    );

    void cm_lt(
        variavel,
        variavel,
        variavel
    );

    void cm_eq(
        variavel,
        variavel,
        variavel
    );

    void cm_neq(
        variavel,
        variavel,
        variavel
    );

    void cm_ge(
        variavel,
        variavel,
        variavel
    );

    void cm_le(
        variavel,
        variavel,
        variavel
    );

    void cm_eqc(
        variavel,
        variavel,
        int
    );

    void cm_gtc(
        variavel,
        variavel,
        int
    );

    void cm_ltc(
        variavel,
        variavel,
        int
    );

    void cm_neqc(
        variavel,
        variavel,
        int
    );

    void cm_gec(
        variavel,
        variavel,
        int
    );

    void cm_lec(
        variavel,
        variavel,
        int
    );
};

#define SET_PROC_WINDOW Janela::Init_Process_Window(&w);

class Janela {
private:
    static MainWindow* ProcessWindow;
    static MainWindow* MemoryWindow;
    
    static pthread_mutex_t OUTPUT;
//    static pthread_mutex_t SIGNAL;

    static std::queue<std::string> strptrs;
    
    static void _print(std::string str);
    static void *_escape_stdout(void* t);
    
protected:

	static void __debufferize__();
	static void __SIGNAL__(); //Finalizar a thread de background

public:
    static void Init_Process_Window(MainWindow* w);
    static void Init_Memory_Window(MainWindow* w);
    
    static void Print(std::string str);

    inline static MainWindow* p_Instance() { return ProcessWindow; }
    inline static MainWindow* m_Instance() { return MemoryWindow; }
};

#endif // VM_H_INCLUDED
