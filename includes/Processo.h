#ifndef PROCESSO_H_INCLUDED
#define PROCESSO_H_INCLUDED

#include "includes.h"

/*
 * Sempre que se usar o comando de imprimir, o stdout ser� preenchido com essa estrutura,
 * e o processo referente entrar� em estado de dormir.
 * Se pid == 0, ent�o ainda existem chars para serem impressos. Quando pid != 0, ent�o a
 * string referente a este pid j� foi impressa, e esse pid sair� do estado de dormir.
 */
typedef struct stdoutchar {
    char caractere;
    PID pid;
} stdoutChar;

typedef struct data {
    union DATATYPE {
        int inteiro;
        char caractere;
        Mutex* mutex; //Deprecated
    } var;

    PID pid; //PID do processo "dono". 0 para globais.
    //Como existe um map na VM, para as globais, sempre buscar antes no escopo do processo (map do processo).
    //Se n�o retornar nenhum iterator, ent�o busca no map da VM.

} DATA;

class Processo {
private:
    PID _PID;

    BancoRegs regs;

    std::vector<Comando> _ComList; //Lista de comandos
    int branchFun; //Aparentemente isso pode sair
    std::stack<int> PC; //Pilha de "PC"

    Estado estado;

    std::stack< std::map<variavel, DATA>* > data;

    unsigned pagina;
    unsigned numPaginas;
    unsigned memoryBegin;
	std::map<variavel, unsigned>* vars;

    bool eq;
    bool gr;
    bool bl;
public:
    char modulo[64]; //Nome do arquivo
    
    void set_numPaginas(
        unsigned 
    );
    void set_pagina(
        unsigned 
    );
    unsigned get_numPaginas();
    unsigned get_pagina();
    void set_memoryBegin(
        unsigned 
    );
    inline unsigned  get_memoryBegin(
    ){
        return this -> memoryBegin;
    }

    void set_vars(
        std::map<variavel, unsigned>* vars
    );

    inline std::map<variavel, unsigned>* get_vars(
    ) {
        return this->vars;
    }

    Processo(
        PID
    );

    void pushData(
    );
    void popData(
    );

    void setPC(
        int
    );
    int getPC(
    );
    void PCPush(
        int
    );
    void PCPop(
    );

    void setBranchF(
        int
    );
    int getBranchF(
    );

    void addComando(
        Comando
    );
    void addVar(
        variavel
    );
    DATA* getvar(
        variavel
    );

    Comando pegaAtomico(
    );

    unsigned int get_comRestantes(
    ) const;

    unsigned int get_comTotais(
    ) const;
    unsigned int get_tempoRestante() { return get_comTotais(); }


    PID getPid(
    );


    void set_estado(
        Estado
    );

    Estado get_estado(
    );

    inline BancoRegs& getBancoRegs(){ return regs;}

    inline bool * check_eq(bool *ret = NULL) {
        if(ret)
            *ret = eq;
        return &eq;
    }
    inline bool * check_gr(bool *ret = NULL) {
        if(ret)
            *ret = gr;
        return &gr;
    }
    inline bool * check_bl(bool *ret = NULL) {
        if(ret)
            *ret = bl;
        return &bl;
    }

};

#endif // PROCESSO_H_INCLUDED
