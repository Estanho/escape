#ifndef TOOLS_H_INCLUDED
#define TOOLS_H_INCLUDED

#include <string.h>
#include <fstream>
#include <string>
#include <iostream>
#include <fstream>

using namespace std;

#define CORRECAO_MEMORIA 0
#define CORRECAO_MEMORIA_ACCESS 0

#if CORRECAO_MEMORIA
	void print_tabs();
#endif 

#if !CORRECAO_MEMORIA && CORRECAO_MEMORIA_ACCESS
	#warning : CORRECAO_MEMORIA_ACCESS depende de CORRECAO_MEMORIA! Impossivel corrigir somente os metodos do tipo access.
#endif 

bool comp(const char* s1, const char* s2);

char* strtolower( char * str);

class Exception {
private:
	char exceptionMsg[512];
	
public:
	Exception(){}
	Exception(int, const char*, const char*);
	
	void showExceptionMessage() const;
	
	~Exception();
/*
	string getMessage(MSGIDs msgId){
		string msg;
		switch (msgId){
			case INIT_ERROR:
				msg = "";
				break;
			case UNKNOWN_EXIT:
				msg = "Simulation terminated by user.";
				break;
			default:
				msg = "Unknown Error Message.";
		}
		return msg;
	}*/
};

void removeColchete( string& str );

#endif // TOOLS_H_INCLUDED
