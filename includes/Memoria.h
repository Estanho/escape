#ifndef MEMORIA_H_INCLUDED
#define MEMORIA_H_INCLUDED

#include <vector>
#include "MemoriaVirtual.h"
#include "MemoriaFisica.h"
#include "FileSystem.h"
#include "../includes/TOOLS.h"


#if CORRECAO_MEMORIA
    extern int tabs_correcao_memoria;
    extern unsigned _tamPagina_;
#endif 


class MemoriaVirtual;
class MemoriaFisica;

class Memoria
{
public : 
    const unsigned tamMemReal;
    const unsigned tamMemVirtual;
    const unsigned tamPagina;
    MemoriaVirtual * memVirtual;//Não é const, mas não mecha mesmo assim.
    MemoriaFisica *  const memFisica;
    FileSystem * const fileSystem;

    Memoria( unsigned tamMemReal, unsigned tamMemVirtual, unsigned tamPagina ,  FileSystem * fileSystem );
	
    //template<class TmemVirtual> ///TmemVirtual deve ser uma herança de memVirtual
    //void initMemVirtual();
    void initMemVirtual(MemoriaVirtual*);
    ~Memoria();
	
    unsigned access_4( unsigned ); //leitura na memoria virtual
    void access_4( unsigned , unsigned );//escrita na memoria virtual 
    bool newPages( unsigned& paginaVirtual, unsigned numeroPaginas );
    void deletePages( unsigned paginaVirtual , unsigned numeroPaginas );

    ///Funções que memoria virtual podem acessar
    unsigned newTempFile()  ;
    void closeFile( unsigned desc )  ;
    
    void rewind( unsigned desc ) ;

    bool fwrite( unsigned pagina,  unsigned desc );
    //Escreve a pagina no arquivo passado, movendo o cursor para o fim do arquivo. Retorna verdadeiro caso não tenha conseguido escrever todos os bytes
    bool fread( unsigned pagina , unsigned desc );
    //Le do arquivo em desc e escreve na pagina, movendo o cursor para o fim do arquivo. Retorna verdadeiro caso não tenha conseguido escrever todos os bytes
    //fread e fwrite podem retornar erros por achar EOF, bloqueio do sistema operacional ou outros erros.      
};


class reg32 {
private:
    int val;
    bool pntr;

public:
    reg32();

    void setVal(
        int
    );
    int getVal(
    );
    void pointer(
        bool p
    ) {
        pntr = p;
    }
};

enum Reg { //No PARssembly todo registrador é de propósito geral
    EAX,
    EBX,
    ECX,
    EDX,
    EEX,
    EFX,
    EGX,
    EHX,
    ENUM_REG_SIZE,
    UNAVAILABLE
};

class BancoRegs {
private:
    std::vector<reg32> REGS;

public:
    BancoRegs();

    reg32* getReg(
        Reg
    );
    Reg getRegEnum(
        const char*
    );
    Reg getRegEnum(
        const char* reg,
        bool& referencia ); ///Esta função é compativel com strings com e sem [], retornando em referecnia se é ou não uma referencia.

};

#endif // MEMORIA_H_INCLUDED
