CXX = g++ -Wall -O2
HEADERS = -I./GUI -I./includes -I/usr/include/qt4/ -I/usr/include/qt4/QtGui/ -I/usr/include/QtGui
LIBS = -L/usr/lib/x86_64-linux-gnu ./GUI-build/libGUI.a -lQtCore -lQtGui -lpthread

SOURCE = Escalonador/Escalonador.cpp Processo/Processo.cpp VM/FileSystem.cpp VM/MemoriaVirtual.cpp VM/MemoriaFisica.cpp VM/Memoria.cpp VM/PARssembly.cpp VM/TOOLS.cpp  
MEMORIA = Paginacao/DumbFifo.cpp # Paginacao/Fifo.cpp 
ESCALONADOR = Escalonador/FIFO.cpp

all: #Compila tudo
	cd GUI-build && qmake-qt4 -project && qmake-qt4 GUI.pro && $(MAKE) && cd .. && ${CXX} ${HEADERS} ${SOURCE} ${MEMORIA} ${ESCALONADOR} VM/VM.cpp main.cpp ${LIBS} -o ESCAPE
nw: #Compila tudo sem warnings
	cd GUI-build && qmake-qt4 -project && qmake-qt4 GUI.pro && $(MAKE) && cd .. && ${CXX} -w ${HEADERS} ${SOURCE} ${MEMORIA} ${ESCALONADOR} VM/VM.cpp main.cpp ${LIBS} -o ESCAPE
ng: #Compila tudo, menos a GUI
	${CXX} ${HEADERS} ${SOURCE} ${MEMORIA} ${ESCALONADOR} VM/VM.cpp main.cpp ${LIBS} -o ESCAPE
ngnw: #Compila tudo, menos a GUI sem warnings
	${CXX} -w ${HEADERS} ${SOURCE} ${MEMORIA} ${ESCALONADOR} VM/VM.cpp main.cpp ${LIBS} -o ESCAPE
nwng: #Compila tudo, menos a GUI sem warnings
	make ngnw
clean: #Apagas todos os arquivos compilados
	rm ESCAPE ; rm GUI-build/*.o ; rm GUI-build/*.a
debug: #Compila em modo de debug
	${CXX} -g ${HEADERS} ${SOURCE} ${MEMORIA} ${ESCALONADOR} VM/VM.cpp main.cpp ${LIBS} -o ESCAPE 
execute: #Executa o projeto
	./ESCAPE
install-qt:
	sudo apt-get install libqt4-* qtcreator 
