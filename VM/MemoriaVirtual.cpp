#include "../includes/MemoriaVirtual.h"

MemoriaVirtual :: ~MemoriaVirtual()
{}

MemoriaVirtual :: MemoriaVirtual(Memoria* memoria) :
memFisica(memoria->memFisica),
memoria(memoria),
tamMemVirtual(memoria->tamMemVirtual),
tamPagina(memoria->tamPagina),
tamMemReal(memoria->tamMemReal)
{
	if(memoria->memVirtual) delete memoria->memVirtual;
	memoria -> memVirtual = this; //Linkagem final, agora memoria também aponta a memoria virtual
}
