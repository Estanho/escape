#include "../includes/FileSystem.h"

FileSystem :: FileSystem() : proxDesc( rand() )
{
}

FileSystem :: ~FileSystem()
{
  for( itMapFile it = arquivosAbertos.begin() ; it != arquivosAbertos.end() ; it++ ) //Fechando todos os arquivos temporarios
  {
    if( fclose( it -> second.c_file ) ) 
    {
      printf("\n\n****ERRO ao fechar arquivos no final do programa\n");
    }
  }
}

unsigned FileSystem ::newTempFile()
{
  File f;
  
  f.c_file = tmpfile() ;///fopen( str , "wb+" );
  
  if( !f.c_file )
  {
    printf( "\n\n****ERRO*** impossivel abrir arquivo temporário\n" );
    exit(1489);
  }
  
  int desc = proxDesc;
  
  proxDesc++;
  
  f.temp = true;
  
  arquivosAbertos.insert(make_pair(desc, f ) );
  return desc;
}

void FileSystem ::closeFile(unsigned desc ) 
{
  itMapFile it =  arquivosAbertos.find( desc );
  
  if( it == arquivosAbertos.end() ) throw Exception(__LINE__,__FILE__, "FileSystem : Descritor de arquivo invalido!");
  
  fclose( it->second.c_file );

  arquivosAbertos.erase(it);
}

FILE* FileSystem ::c_file( unsigned desc ) 
{
  itMapFile it =  arquivosAbertos.find( desc );
  
  if( it == arquivosAbertos.end() ) return NULL;
  
  return  it->second.c_file;
}



void FileSystem :: rewind( unsigned desc ) 
{
  std::rewind( this -> c_file(desc) );
}

size_t FileSystem ::fwrite( void* vetor, size_t bytes, unsigned desc ) 
{
  return ::fwrite( vetor , 1 , bytes, this -> c_file( desc )  );
}

size_t FileSystem ::fread( void * vetor, size_t bytes , unsigned desc ) 
{
  return ::fread ( vetor, 1 , bytes, this->c_file(desc) );
}