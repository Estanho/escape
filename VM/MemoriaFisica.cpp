#include "../includes/MemoriaFisica.h"

MemoriaFisica :: MemoriaFisica( unsigned tamMemoria ) :
tamMemoria( tamMemoria )
{
	memoria_array = (char*) malloc( tamMemoria );
	if(!memoria_array) {
		printf("\n\n***ERRO : Impossivel alocar memoria física\n");
		fflush(stdout);
		exit(1); 
	}
}



MemoriaFisica :: ~MemoriaFisica()
{
	free(memoria_array);
}


unsigned MemoriaFisica :: access_4( unsigned adress ) //leitura na memoria fisica
{
	unsigned a = *( (unsigned*) (memoria_array  + adress) );

	 #if ( CORRECAO_MEMORIA && CORRECAO_MEMORIA_ACCESS )
        print_tabs();
        printf("=MF= access_4( %x[%x:%x] ) = %u \n",adress,adress/_tamPagina_, adress%_tamPagina_ , a);
    #endif
	
	return a;        
}
void MemoriaFisica :: access_4( unsigned adress , unsigned value )//escrita na memoria fisica 
{
	 #if CORRECAO_MEMORIA && CORRECAO_MEMORIA_ACCESS 
        print_tabs();
        printf("=MF= access_4( %x[%x:%x] , %u ) \n",adress,adress/_tamPagina_, adress%_tamPagina_ , value);
    #endif

	*( (unsigned*) (memoria_array  + adress) ) = value;
}