#include "../includes/PARssembly.h"
#include "../includes/Processo.h"
#include "../includes/TOOLS.h"
#include <fstream>
#include <string.h>


unsigned codigo_line;

#define DEBUG 0

#define DEBUG_READ_LINE 0

#define DEBUG_STEP_BY_STEP 0

#define ERROR_COMPILATION() \
    printf("ERRO DE COMPILAÇÃO na linha do %d do seu .par.\n\nPara mais informações a linha do compilador que encontrou o seu erro foi %d do arquivo escape/VM/PARssembly.cpp\n", codigo_line, __LINE__); \
    fflush(stdout); \
    return false

#define ERROR_OUT_OF_MEMORY() \
  printf("\***ERRO Falta de memoria\n"); \
  fflush(stdout); \
  return false



void ProcBuffer::DelProc(std::string in)
{
	string path = "PARnasm/" + in;
    proc.erase(path);
}

bool ProcBuffer::AddProc(std::string in)
{
    vector<char> dataValues;
    Dummy_Proc newp;
    
    strcpy(newp.modulo, &(in.c_str()[8]));
    
    newp.data = NULL;
    newp.tam_data = 0;
    newp.tam_vars = 0;
    
    if (proc.count(in))   //Se já existe
    {
        cout << "ERRO: Processo já bufferizado: " << in << endl;
        return false;
    }

    char line[1024];
    char COM1[32], COM2[32], COM3[2048];
    std::ifstream is(in.c_str(), ifstream::in);
    bool dataIn = false, bssIn = false, textIn = false;


    if (is.rdstate() & std::ifstream::failbit)
    {
        cout << "ERRO: Arquivo Inválido: " << in << endl;
        return false;
    }

    vector<Comando>* comVec = new vector<Comando>;

    //proc.insert(pair< std::string, vector<Comando*>* >(in, comVec));

    Comando* com;

    codigo_line = 0;

    map<string,unsigned> * memory_bss = new map<string,unsigned>;///Data e bss, depois precisam ser juntos.
    map<string,unsigned> * memory_data = new map<string,unsigned>;
    unsigned count_memory_data = 0;
    unsigned count_memory_bss = 0;
    unsigned size_on_memory = 0;

    unsigned int PC = 0;

	std::ifstream is2(in.c_str(), ifstream::in);

	while (is2.getline(line, 1024)) { //Parse para setar os labels de código

        sscanf(line, "%s %s %[^\n]", COM1, COM2, COM3);
        if(section(COM1))
        {
            if(data(COM2))
            {
                dataIn = true;
                bssIn = false;
                textIn = false;
            }
            else if(bss(COM2))
            {
                bssIn = true;
                dataIn = false;
                textIn = false;
            }
            else if(text(COM2))
            {
                textIn = true;
                bssIn = false;
                dataIn = false;
            }
            continue;
        }

        if(textIn)
        {
            if(_label(line))   //label:
            {
				COM1[strlen(COM1)-1] = '\0';

                newp.LabelPc.insert( pair<std::string, unsigned int>( std::string(COM1), PC) );
            }
            else if( line[0] == '\0' )
            {
                continue;
            }
			PC++;
		}
	}
	is2.close();

    if (is.rdstate() & std::ifstream::failbit)
    {
        return false;
    }

    while (is.getline(line, 1024))
    {
        codigo_line++;

        sscanf(line, "%s %s %[^\n]", COM1, COM2, COM3);
        if(section(COM1))
        {
            if(data(COM2))
            {
                dataIn = true;
                bssIn = false;
                textIn = false;
            }
            else if(bss(COM2))
            {
                bssIn = true;
                dataIn = false;
                textIn = false;
            }
            else if(text(COM2))
            {
                textIn = true;
                bssIn = false;
                dataIn = false;
            }
            continue;
        }

        if(textIn)
        {
            com = new Comando;
            com->args = NULL;
            if(_mov(line))   //MOV reg32, reg/m32
            {
                com->COM = Comando::MOV;

                com->args = new Comando::ARGS[2];
                if(COM2[0] == '[') COM2[5] = '\0';
                else COM2[3] = '\0';
                com->args[0].var = new variavel(COM2);
                com->args[1].var = new variavel(COM3);
            }
            else if(_movi(line))   //MOVI reg32, imm32
            {
                com->COM = Comando::MOVI;

                com->args = new Comando::ARGS[2];
                COM2[3] = '\0';
                com->args[0].var = new variavel(COM2);
                sscanf(COM3, "%d", &(com->args[1].imm));
            }
            else if(_add(line))   //ADD reg32, reg32
            {
                com->COM = Comando::ADD;

                com->args = new Comando::ARGS[2];
                COM2[3] = '\0';
                com->args[0].var = new variavel(COM2);
                com->args[1].var = new variavel(COM3);
            }
            else if(_sub(line))   //SUB reg32, reg32
            {
                com->COM = Comando::SUB;

                com->args = new Comando::ARGS[2];
                COM2[3] = '\0';
                com->args[0].var = new variavel(COM2);
                com->args[1].var = new variavel(COM3);
            }
            else if(_mul(line))   //MUL reg32 (UNSIGNED) [reg32 := EAX*reg32]
            {
                com->COM = Comando::MUL;

                com->args = new Comando::ARGS[1];
                com->args[0].var = new variavel(COM2);
            }
            else if(_imul(line))   //MUL reg32 (SIGNED)
            {
                com->COM = Comando::IMUL;

                com->args = new Comando::ARGS[1];
                com->args[0].var = new variavel(COM2);
            }
            else if(_div(line))   //DIV reg32 (UNSIGNED)
            {
                com->COM = Comando::DIV;

                com->args = new Comando::ARGS[1];
                com->args[0].var = new variavel(COM2);
            }
            else if(_idiv(line))   //DIV reg32 (SIGNED)
            {
                com->COM = Comando::IDIV;

                com->args = new Comando::ARGS[1];
                com->args[0].var = new variavel(COM2);
            }
            else if(_and(line))   //AND reg32, reg32
            {
                com->COM = Comando::AND;

                com->args = new Comando::ARGS[2];
                COM2[3] = '\0';
                com->args[0].var = new variavel(COM2);
                com->args[1].var = new variavel(COM3);
            }
            else if(_or(line))   //OR reg32, reg32
            {
                com->COM = Comando::OR;

                com->args = new Comando::ARGS[2];
                COM2[3] = '\0';
                com->args[0].var = new variavel(COM2);
                com->args[1].var = new variavel(COM3);
            }
            else if(_xor(line))   //XOR reg32, reg32
            {
                com->COM = Comando::XOR;

                com->args = new Comando::ARGS[2];
                COM2[3] = '\0';
                com->args[0].var = new variavel(COM2);
                com->args[1].var = new variavel(COM3);
            }
            else if(_not(line))   //NOT reg32
            {
                com->COM = Comando::NOT;

                com->args = new Comando::ARGS[1];
                com->args[0].var = new variavel(COM2);
            }
            else if(_push(line))   //PUSH reg32
            {
                com->COM = Comando::PUSH;

                com->args = new Comando::ARGS[1];
                com->args[0].var = new variavel(COM2);
            }
            else if(_call(line))   //CALL imm
            {
                com->COM = Comando::CALL;

                com->args = new Comando::ARGS[1];
                com->args[0].var = new variavel(COM2);
            }
            else if(_ret(line))   //RET
            {
                com->COM = Comando::RET;
            }
            else if(_cmp(line))   //CMP reg32, reg32
            {
                com->COM = Comando::CMP;

                com->args = new Comando::ARGS[2];
                COM2[3] = '\0';
                com->args[0].var = new variavel(COM2);
                com->args[1].var = new variavel(COM3);
            }
            else if(_jg(line))   //JG imm
            {
                com->COM = Comando::JG;

                com->args = new Comando::ARGS[1];
                com->args[0].imm = newp.LabelPc.find(COM2)->second;
            }
            else if(_jge(line))   //JGE imm
            {
                com->COM = Comando::JGE;

                com->args = new Comando::ARGS[1];
                com->args[0].imm = newp.LabelPc.find(COM2)->second;
            }
            else if(_jl(line))   //JL imm
            {
                com->COM = Comando::JL;

                com->args = new Comando::ARGS[1];
                com->args[0].imm = newp.LabelPc.find(COM2)->second;
            }
            else if(_jle(line))   //JLE imm
            {
                com->COM = Comando::JLE;

                com->args = new Comando::ARGS[1];
                com->args[0].imm = newp.LabelPc.find(COM2)->second;
            }
            else if(_je(line))   //JE imm
            {
                com->COM = Comando::JE;

                com->args = new Comando::ARGS[1];
                com->args[0].imm = newp.LabelPc.find(COM2)->second;
            }
            else if(_jne(line))   //JNE imm
            {
                com->COM = Comando::JNE;

                com->args = new Comando::ARGS[1];
                com->args[0].imm = newp.LabelPc.find(COM2)->second;
            }
            else if(_ja(line))   //JA imm
            {
                com->COM = Comando::JA;

                com->args = new Comando::ARGS[1];
                com->args[0].imm = newp.LabelPc.find(COM2)->second;
            }
            else if(_jae(line))   //JAE imm
            {
                com->COM = Comando::JAE;

                com->args = new Comando::ARGS[1];
                com->args[0].imm = newp.LabelPc.find(COM2)->second;
            }
            else if(_jb(line))   //JB imm
            {
                com->COM = Comando::JB;

                com->args = new Comando::ARGS[1];
                com->args[0].imm = newp.LabelPc.find(COM2)->second;
            }
            else if(_jbe(line))   //JBE imm
            {
                com->COM = Comando::JBE;

                com->args = new Comando::ARGS[1];
                com->args[0].imm = newp.LabelPc.find(COM2)->second;
            }
            else if(_jmp(line))   //JMP imm
            {
                com->COM = Comando::JMP;

                com->args = new Comando::ARGS[1];
                com->args[0].imm = newp.LabelPc.find(COM2)->second;
            }
            else if(_print(line))   //PRT imm
            {
                com->COM = Comando::PRINT;

                com->args = new Comando::ARGS[1];
                com->args[0].var = new variavel(COM2);
            }
            else if(_label(line))   //label:
            {
				continue;
            }
            else if( line[0] != '\0' )
            {
                ERROR_COMPILATION();
            }
            else {
                continue;
            }

            comVec->push_back(*com);

        }
        else if(dataIn)
        {
            char* c;
            if(COM1[0] != '\0')
            {
				#if DEBUG_READ_LINE
					printf("->%s\n",COM1);
				#endif
				if( *(c = &COM1[strlen(COM1)-1]) == ':' )
                {
                    *c = '\0';

                    map<string,unsigned>::iterator it = memory_data -> find( COM1 );
                    map<string,unsigned>::iterator it2 = memory_bss -> find( COM1 );
                    if( it != memory_data->end() || it2 != memory_bss->end() )
                    {
                        ERROR_COMPILATION();
                    }

                    strtolower(COM2);
                    size_on_memory = 0;

                    #if DEBUG
                    printf("-%s-\n",COM2);
                    #endif

                    if( !strcmp(COM2,"db" ) )
                    {
                        #if DEBUG
                        printf("-%s-\n",COM3);
                        #endif

                        if( COM3[0] == '\"' )
                        {
                            size_on_memory = strlen(COM3) - 1;
                            memory_data -> insert( make_pair( string(COM1) , count_memory_data ) );

                            COM3[size_on_memory] = '\0';
                            //size_on_memory--;

                            ///COLOCAR NA MEMORIA!!! ( COM3 + 1 )
                            for( int i = 1 ; COM3[i] != '\0' ; i++){
                                if(COM3[i] == '\\'){
                                    size_on_memory--;
                                    switch(COM3[++i]){
                                        case '\\':  {
                                                        ERROR_COMPILATION();
                                                    }
                                            break;
                                        case 'n': 
                                            dataValues.push_back('\n');
                                            break;
                                        case 'r': 
                                            dataValues.push_back('\r');
                                            break;
                                        case '\"': 
                                            dataValues.push_back('\"');
                                            break;
                                        case '\'': 
                                            dataValues.push_back('\'');
                                            break;
                                        case 't': 
                                            dataValues.push_back('\t');
                                            break;
                                        case 'a': 
                                            dataValues.push_back('\a');
                                            break;
                                        case 'b': 
                                            dataValues.push_back('\b');
                                            break;
                                        case 'f':       
                                            dataValues.push_back('\f');
                                            break;
                                        case 'v': 
                                            dataValues.push_back('\v');
                                            break;
                                        default:{
                                                    ERROR_COMPILATION();
                                                }
                                            break;

                                    }
                                }else{
                                    dataValues.push_back(COM3[i]);      
                                }
                            }
                            
                            dataValues.push_back('\0');

                            #if DEBUG
                            printf("data %s %d - %d (%s)\n" , COM1, count_memory_data, count_memory_data + size_on_memory - 1 , COM3 + 1);
                            #endif

                            count_memory_data += size_on_memory;
                        }
                        else
                        {
                            ERROR_COMPILATION();
                        }
                    }
                    else if( !strcmp(COM2,"dw" ) )
                    {
                        memory_data -> insert( make_pair( string(COM1) , count_memory_data ) );

                        char* _COM3 = COM3;
                        while( _COM3 != '\0' )
                        {
                            short int num;
                            if( sscanf(_COM3,"%hd ",&num ) == 0 ) break;

                            size_on_memory += 2;

                            #if DEBUG
                            printf("data %s %d - %d %hd\n" , COM1, count_memory_data + size_on_memory - 2, count_memory_data + size_on_memory - 1 , num);
                            #endif

                            ///COLOCAR NA MEMORIA!!! ( num)
            			    for(int i = 0 ; i < 2 ; i ++ )
            			    {
            			      char c = num;
            			      dataValues.push_back(c);
            			      num >>= 8;
            			    }
                           
                           
                            _COM3 = strchr(_COM3, ',' );
                            if( !_COM3 ) break;
                            _COM3++;

                            #if DEBUG_STEP_BY_STEP
                            getchar();
                            #endif
                        }

                        count_memory_data += size_on_memory;
                    }
                    else if( !strcmp(COM2,"dd" ) )
                    {
                        memory_data -> insert( make_pair( string(COM1) , count_memory_data ) );

                        char* _COM3 = COM3;
                        while( _COM3 != '\0' )
                        {
                            int num;
                            if( sscanf(_COM3,"%d ",&num ) == 0 ) break;
                            ///COLOCAR NA MEMORIA!!! ( num)

                            size_on_memory += 4;

                            #if DEBUG
                            printf("data %s %d - %d %d\n" ,COM1, count_memory_data + size_on_memory - 4, count_memory_data + size_on_memory - 1 , num);
                            #endif

            			    for(int i = 0 ; i < 4 ; i ++ )
            			    {
            			      char c = num;
            			      dataValues.push_back(c);
            			      num >>= 8;
            			    }
			    


                            _COM3 = strchr(_COM3, ',' );
                            if( !_COM3 ) break;
                            _COM3++;

                            #if DEBUG_STEP_BY_STEP
                            getchar();
                            #endif
                        }

                        count_memory_data += size_on_memory;
                    }
                    else if( !strcmp(COM2,"dq" ) )
                    {
                        memory_data -> insert( make_pair( string(COM1) , count_memory_data ) );

                        char* _COM3 = COM3;
                        while( _COM3 != '\0' )
                        {
                            long long int num;
                            if( sscanf(_COM3,"%Ld ",&num ) == 0 ) break;
                            ///COLOCAR NA MEMORIA!!! ( num)

                            size_on_memory += 8;
                            
                            #if DEBUG
                            printf("data %s %d - %d %Ld\n" ,COM1, count_memory_data + size_on_memory - 8, count_memory_data + size_on_memory - 1 , num);
                            #endif

            			    for(int i = 0 ; i < 8 ; i ++ )
            			    {
            			      char c = num;
            			      dataValues.push_back(c);
            			      num >>= 8;
            			    }
			    


                            _COM3 = strchr(_COM3, ',' );
                            if( !_COM3 ) break;
                            _COM3++;

                            #if DEBUG_STEP_BY_STEP
                            getchar();
                            #endif
                        }

                        count_memory_data += size_on_memory;
                    }
                    else
                    {
                        ERROR_COMPILATION();
                    }
                }
            }
		}
        else if(bssIn)
        {
            char* c;
            if(COM1[0] != '\0')
            {
                if( *(c = &COM1[strlen(COM1)-1]) == ':' )
                {
                    *c = '\0';

                    map<string,unsigned>::iterator it = memory_data -> find( COM1 );
                    map<string,unsigned>::iterator it2 = memory_bss -> find( COM1 );
                    if( it != memory_data->end() || it2 != memory_bss->end() )
                    {
                        ERROR_COMPILATION();
                    }

                    strtolower(COM2);
                    unsigned size_on_memory = 0;

                    #if DEBUG
                    printf("%d-%s-%s-%s-\n",codigo_line, COM1,COM2,COM3);
                    #endif

                    if( sscanf(COM3,"%u ", &size_on_memory) != 1 ) //Lendo o tamanho do vetor que será alocado, se não consegue erro de compilação.
                    {
                        ERROR_COMPILATION();
                    }

                    if( !strcmp(COM2, "resw") ) //Procurnado o tipo do vetor, byte, word, double ou quad, Se não for nenhum desses ERRO DE COMPILAÇÃO.
                    {
                        size_on_memory *= 2;
                        #if DEBUG
                        printf("bss resw ");
                        #endif
                    }
                    else if( !strcmp(COM2, "resd") )
                    {
                        size_on_memory *= 4;
                        #if DEBUG
                        printf("bss resd ");
                        #endif
                    }
                    else if( !strcmp(COM2, "resq") )
                    {
                        size_on_memory *= 8;
                        #if DEBUG
                        printf("bss resq ");
                        #endif
                    }
                    else if( strcmp(COM2, "resb") ) ///E não é resb. Lembrando que resb não precisa fazer mais nada visto que é 1 byte por casa, logo *= 1.
                    {
                        ERROR_COMPILATION();
                    }
                    #if DEBUG
                    else
                    {
                        printf("bss resb ");
                    }
                    #endif

                    memory_bss -> insert( make_pair( string(COM1) , count_memory_bss ) );

                    #if DEBUG
                    printf(" %s %d - %d\n" ,COM1, count_memory_bss, count_memory_bss + size_on_memory - 1 );
                    #endif

                    count_memory_bss += size_on_memory;
                    ///Nao precisa escrever na memoria, mas precisa reservar o espaço na memoria.
                }
            }
        }

    }
	
    if( count_memory_data%4 ) //Truncamento para multiplos de 4.
    {
		count_memory_data += 4 - (count_memory_data%4);
    }
    
	unsigned count_memory = count_memory_data + count_memory_bss;
    
    newp.tam_data = count_memory_data;
    newp.data = new char[ newp.tam_data ];
    
    newp.tam_vars = count_memory;
    
    unsigned tam = dataValues.size() -5;
    for( unsigned i = 0 ; i < tam ; i++ )
    {
		newp.data[i] = dataValues[i];
    }
    
    #if DEBUG
    	printf("\n\n");
    	for(unsigned int i = 0 ; i < newp.tam_data ; i++)
    	{
    		//printf("[%c,%x] ",newp.data[i] , newp.data[i] ); //
    		printf("%x.", (char unsigned)  newp.data[i]  );
    	}
    	printf("\n\n");
    #endif
    
    map<string,unsigned> * memory_acess = memory_data; //Apenas mudando o nome da mesma estrutura

    map<string,unsigned>::iterator it = memory_bss->begin(); ///Juntando bss e data. Bss será tratado como os bytes seguintes à data, logo irei somar em cada endereço do bss += count_memory_data.
    while( it != memory_bss -> end() )
    {
        (it -> second) += count_memory_data;
        memory_acess -> insert( *it );
        it++;
    }

    #if DEBUG
        cout << "Memory Data = " << count_memory_data << "\tMemory bss = " << count_memory_bss << "\t Total Memory = " << count_memory << endl;
    #endif

    delete memory_bss;///Bss não é mais util, agora memory_acess tem os dois maps nele.


    #if DEBUG
        printf("\n\n\n\n");

        it = memory_acess -> begin();
        while( it != memory_acess -> end() )
        {
            cout << it -> first << "-" << it -> second  << endl;
            it++;
        }
    #endif

    newp._ComList = *comVec;
    delete comVec;
    newp.vars = memory_acess;
    
    ( proc.insert(std::pair<std::string, Dummy_Proc>(in, newp)) ).first->second.deletable = true;
    
    //throw Exception( __LINE__,__FILE__, "adfasf!");
    #if DEBUG
	   printf("SAIU\n");
    #endif 
    
    return true;
}

bool ProcBuffer::section(char* s)
{
    return comp(s, (char*)"section") == 0 ? true : false;
}
bool ProcBuffer::data(char* s)
{
    return comp(s, (char*)".data") == 0 ? true : false;
}
bool ProcBuffer::bss(char* s)
{
    return comp(s, (char*)".bss") == 0 ? true : false;
}
bool ProcBuffer::text(char* s)
{
    return comp(s, (char*)".text") == 0 ? true : false;
}
bool ProcBuffer::_mov(char* s)
{
    return comp(s, (char*)"mov") == 0 ? true : false;
}
bool ProcBuffer::_movi(char* s)
{
    return comp(s, (char*)"movi") == 0 ? true : false;
}
bool ProcBuffer::_add(char* s)
{
    return comp(s, (char*)"add") == 0 ? true : false;
}
bool ProcBuffer::_sub(char* s)
{
    return comp(s, (char*)"sub") == 0 ? true : false;
}
bool ProcBuffer::_mul(char* s)
{
    return comp(s, (char*)"mul") == 0 ? true : false;
}
bool ProcBuffer::_imul(char* s)
{
    return comp(s, (char*)"imul") == 0 ? true : false;
}
bool ProcBuffer::_div(char* s)
{
    return comp(s, (char*)"div") == 0 ? true : false;
}
bool ProcBuffer::_idiv(char* s)
{
    return comp(s, (char*)"idiv") == 0 ? true : false;
}
bool ProcBuffer::_and(char* s)
{
    return comp(s, (char*)"and") == 0 ? true : false;
}
bool ProcBuffer::_or(char* s)
{
    return comp(s, (char*)"or") == 0 ? true : false;
}
bool ProcBuffer::_xor(char* s)
{
    return comp(s, (char*)"xor") == 0 ? true : false;
}
bool ProcBuffer::_not(char* s)
{
    return comp(s, (char*)"not") == 0 ? true : false;
}
bool ProcBuffer::_push(char* s)
{
    return comp(s, (char*)"push") == 0 ? true : false;
}
bool ProcBuffer::_pop(char* s)
{
    return comp(s, (char*)"pop") == 0 ? true : false;
}
bool ProcBuffer::_call(char* s)
{
    return comp(s, (char*)"call") == 0 ? true : false;
}
bool ProcBuffer::_ret(char* s)
{
    return comp(s, (char*)"ret") == 0 ? true : false;
}
bool ProcBuffer::_cmp(char* s)
{
    return comp(s, (char*)"cmp") == 0 ? true : false;
}
bool ProcBuffer::_jg(char* s)
{
    return comp(s, (char*)"jg") == 0 ? true : false;
}
bool ProcBuffer::_jge(char* s)
{
    return comp(s, (char*)"jge") == 0 ? true : false;
}
bool ProcBuffer::_jl(char* s)
{
    return comp(s, (char*)"jl") == 0 ? true : false;
}
bool ProcBuffer::_jle(char* s)
{
    return comp(s, (char*)"jle") == 0 ? true : false;
}
bool ProcBuffer::_je(char* s)
{
    return comp(s, (char*)"je") == 0 ? true : false;
}
bool ProcBuffer::_jne(char* s)
{
    return comp(s, (char*)"jne") == 0 ? true : false;
}
bool ProcBuffer::_ja(char* s)
{
    return comp(s, (char*)"ja") == 0 ? true : false;
}
bool ProcBuffer::_jae(char* s)
{
    return comp(s, (char*)"jae") == 0 ? true : false;
}
bool ProcBuffer::_jb(char* s)
{
    return comp(s, (char*)"jb") == 0 ? true : false;
}
bool ProcBuffer::_jbe(char* s)
{
    return comp(s, (char*)"jbe") == 0 ? true : false;
}
bool ProcBuffer::_jmp(char* s)
{
    return comp(s, (char*)"jmp") == 0 ? true : false;
}
bool ProcBuffer::_print(char* s)
{
    return comp(s, (char*)"prt") == 0 ? true : false;
}
bool ProcBuffer::_label(char* s)
{
    return s[strlen(s)-1] == ':';
}

bool ProcBuffer::GetProc(std::string procName, Processo* pProc , Memoria* memoria )
{
    Comando end_proc;
    //Começa a botar os comandos no processo
    Comando* c;
    Dummy_Proc& dp = proc.find(procName)->second;
    ///Alocando memoria
    unsigned pagina;
    unsigned memoryBegin;
    unsigned numPaginas = dp.tam_vars/(memoria->tamPagina);
    if( dp.tam_vars%(memoria->tamPagina) ) numPaginas++;
    if( !memoria -> newPages( pagina , numPaginas) ) return false;
        //Caso não tenha alocado retorna falso, ou seja não foi possivel iniciar esse processo.
    pProc -> set_numPaginas(numPaginas);
    pProc -> set_pagina(pagina);
    memoryBegin = pagina * memoria -> tamPagina;
    pProc -> set_memoryBegin(memoryBegin);
    ///Passando as variaveis do data para a memoria
    for( unsigned i = 0; i < dp.tam_data ; i += 4)
    {
        memoria -> access_4( i + memoryBegin , *( (unsigned *) (dp.data + i)  )   );
    }
    ///Passando mapa de variaveis
    pProc -> set_vars( dp.vars );

    std::vector<Comando>::iterator it_list = dp._ComList.begin();

    while(it_list != dp._ComList.end()) {
        #if DEBUG
        cout << it_list->COM << endl;
        #endif
        
        c = &(*it_list);

        pProc->addComando(*c);
        ++it_list;
    }
    end_proc.COM = Comando::END_PROC;
    end_proc.args = NULL;
    pProc->addComando(end_proc);

	strcpy(pProc->modulo, dp.modulo);

    return true;
}

ProcBuffer PARssembly_Reader::ps;
bool PARssembly_Reader::ReadProc(std::string in)
{
//    char* input = new char[512];

    Janela::Print(string("Lendo processo..."));

    if(ps.AddProc(in))
    {
	    Janela::Print(string("Processo lido com sucesso!"));
        
        return true;
    }
    else 
	{
		Janela::Print(string("Processo não adicionado. Erro de compilação!!"));
	}
    return false;
}
void PARssembly_Reader::UnReadProc(std::string in)
{
    ps.DelProc(in);
}

ProcBuffer* PARssembly_Reader::getPbuffer()
{
    return &ps;
}
