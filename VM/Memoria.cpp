#include "../includes/Memoria.h"
#include "../includes/TOOLS.h"

#define TEST_FREAD_FWRITE 0 
///Testa fread e fwrite entre Memoria e MemoriaFisica, o teste ocorre durante o construtor de Memoria , logo após o construtor de MemoriaFisica

Memoria ::  Memoria(unsigned tamMemReal, unsigned tamMemVirtual, unsigned tamPagina ,  FileSystem * fileSystem ) : 
    tamMemReal(tamMemReal),
    tamMemVirtual( tamMemVirtual),
    tamPagina( tamPagina),
    memVirtual(NULL),
    memFisica ( new MemoriaFisica (tamMemReal) ) , 
    fileSystem( fileSystem )
{
    #if CORRECAO_MEMORIA
        _tamPagina_ = tamPagina;
    #endif 


   #if TEST_FREAD_FWRITE
    vector<unsigned> dados;
    bool falha = false;
    printf("\n\n Escrevendo na memoria Fisica\n\n");
    for(unsigned i = 0 ; i < tamPagina ; i+= 4)
    {
        unsigned data = rand();
        memFisica->access_4( i , data );
        printf("%u : %u",i,data);
        printf( i%12 ? "\t\t" : "\n" );
        dados.push_back(data);
    }

    printf("\n\n Lendo da memoria Fisica\n\n");
    for(unsigned i = 0 , cont = 0 ; i < tamPagina ; i+= 4 , cont++)
    {
        unsigned data;
        data = memFisica->access_4( i  );
        printf("%u : %u",i,data);
        printf( i%12 ? "\t\t" : "\n" );
        
        if( dados[cont] != data )
            falha = true;
    }

    if( !falha ) printf("\n>>>>>>>>>>>>>MEMORIA INTACTA\n");
    else printf("\n>>>>>>>>>>>>>FALHA NA MEMORIA\n");

    //Escrevendo ela num arquivo
    unsigned desc = newTempFile();
    fwrite( 0, desc);//Pagina zero escrita
    rewind(desc);

    ///Zerando aquela pagina
    for( unsigned i = 0; i < tamPagina ; i+= 4)
        memFisica->access_4(i,0);

    fread(0,desc);///Lendo de volta do arquivo
    closeFile(desc);//Fechando arquivo ( e excuindo por consequencia )

    printf("\n\n Lendo da memoria depois de fread e fwrite Fisica\n\n");
    for(unsigned i = 0 , cont = 0 ; i < tamPagina ; i+= 4 , cont++)
    {
        unsigned data;
        data = memFisica->access_4( i  );
        printf("%u : %u",i,data);
        printf( i%12 ? "\t\t" : "\n" );

        if( dados[cont] != data )
            falha = true;
    }

    if( !falha ) printf("\n>>>>>>>>>>>>>MEMORIA INTACTA\n");
    else printf("\n>>>>>>>>>>>>>FALHA NA MEMORIA\n");

    #endif 
} 

/*template<class TmemVirtual>  ///TmemVirtual deve ser uma herança de memVirtual
void Memoria::initMemVirtual()
{
    memVirtual = new TmemVirtual( tamMemReal, tamMemVirtual, tamPagina, memFisica, this);
}*/
/*void Memoria::initMemVirtual(MemoriaVirtual* m)
{
	//if(memVirtual) delete memVirtual;
	
	cout << endl << endl << memVirtual << endl << endl << m;
    memVirtual = (MemoriaVirtual*)m;
}*/

Memoria :: ~Memoria()
{
    delete memVirtual;
    delete memFisica;
}

unsigned Memoria ::  access_4( unsigned adress)
{
    #if (CORRECAO_MEMORIA && CORRECAO_MEMORIA_ACCESS )
        unsigned ret;
    
        print_tabs();
        tabs_correcao_memoria++;
        printf(">MEM> access_4( %x[%x:%x] )\n",adress,adress/tamPagina, adress%tamPagina);
        
        ret = memVirtual->access_4(adress);

        tabs_correcao_memoria--;
        print_tabs();
        printf("<MEM< ( %u )\n",ret);
        return ret;
    #else
        return memVirtual->access_4(adress); 
    #endif 
}
void Memoria ::  access_4( unsigned adress, unsigned value)
{
    #if  CORRECAO_MEMORIA && CORRECAO_MEMORIA_ACCESS 
        print_tabs();
        tabs_correcao_memoria++;

        printf(">MEM> access_4( %x[%x:%x] , %u )\n",adress,adress/tamPagina, adress%tamPagina , value );
    #endif 
    
    memVirtual->access_4(adress,value);

    #if CORRECAO_MEMORIA && CORRECAO_MEMORIA_ACCESS
        tabs_correcao_memoria--;
        print_tabs();
        printf("<MEM<\n");
    #endif 
}

bool Memoria::newPages(unsigned& paginaVirtual,unsigned numeroPaginas){
    #if CORRECAO_MEMORIA
        print_tabs();
        tabs_correcao_memoria++;

        printf(">MEM> newPages( ? , %u )\n" , numeroPaginas );
        
        bool b = memVirtual -> newPages(paginaVirtual,numeroPaginas);

        tabs_correcao_memoria--;
        print_tabs();
        if( b ) 
            printf("<MEM< newPages( %x , %u ) = true\n",paginaVirtual,numeroPaginas);
        else 
            printf("<MEM< newPages( !NEGADO! , %u ) = false\n",numeroPaginas);
        return b;
    #else  
        return memVirtual -> newPages(paginaVirtual,numeroPaginas);   
    #endif
}

void Memoria::deletePages(unsigned paginaVirtual, unsigned numeroPaginas){
    #if CORRECAO_MEMORIA
        print_tabs();
        tabs_correcao_memoria++;
        printf(">MEM> deletePages( %x , %u )\n" , paginaVirtual, numeroPaginas );
    #endif 

    memVirtual -> deletePages( paginaVirtual, numeroPaginas);

    #if CORRECAO_MEMORIA
        tabs_correcao_memoria--;
        print_tabs();
        printf("<MEM<\n");
    #endif 
}


unsigned Memoria::newTempFile() 
{
    #if CORRECAO_MEMORIA
        unsigned ret = fileSystem -> newTempFile();

        print_tabs();
        printf("=FS= newTempFile() = %x\n", ret );

        return ret;
    #else 
        return fileSystem -> newTempFile();
    #endif  
}

void Memoria  ::closeFile( unsigned desc ) 
{
    #if CORRECAO_MEMORIA
        print_tabs();
        printf("=FS= closeFile(%x)\n", desc );        
    #endif
    return fileSystem -> closeFile( desc );
}

void Memoria  ::rewind( unsigned desc ) 
{
    #if CORRECAO_MEMORIA
        print_tabs();
        printf("=FS= rewind(%x)\n", desc );        
    #endif
    return fileSystem -> rewind( desc );
}

bool Memoria ::fwrite( unsigned pagina,  unsigned desc ) 
{
    #if CORRECAO_MEMORIA
        void* vetor = (void*)(memFisica->memoria_array + pagina*tamPagina);
        unsigned t = fileSystem -> fwrite( vetor , tamPagina,  desc );
        bool ret = t != tamPagina;

        print_tabs();
        printf("=FS= fwrite(%x,%x) = ", pagina, desc );
        printf( ret ? "true\n" : "false\n" );

        return ret;
    #else 
        void* vetor = (void*)(memFisica->memoria_array + pagina*tamPagina);
        unsigned t = fileSystem -> fwrite( vetor , tamPagina,  desc );
        return t != tamPagina;
    #endif 
}
bool Memoria  ::fread( unsigned pagina,  unsigned desc ) 
{
    #if CORRECAO_MEMORIA
        void* vetor = (void*)(memFisica->memoria_array + pagina*tamPagina);
        unsigned t = fileSystem->fread( vetor, tamPagina,  desc );
        return t != tamPagina;        
        bool ret = t != tamPagina;

        print_tabs();
        printf("=FS= fread(%x,%x) = ", pagina, desc );
        printf( ret ? "true\n" : "false\n" );

        return ret;
    #else 
        void* vetor = (void*)(memFisica->memoria_array + pagina*tamPagina);
        unsigned t = fileSystem->fread( vetor, tamPagina,  desc );
        return t != tamPagina;
    #endif 


}



void reg32::setVal(int v) {
    val = v;
}
int reg32::getVal() {
    return val;
}
reg32::reg32() {
    val = 0;
    pntr = false;
}

BancoRegs::BancoRegs() {
    REGS.resize(ENUM_REG_SIZE);
}
reg32* BancoRegs::getReg(Reg reg) {
    if( reg == UNAVAILABLE ) 
        throw Exception( __LINE__, __FILE__, "Reg inexistente" );
    return &(REGS[reg]);
}
Reg BancoRegs::getRegEnum(const char* reg) {
    if(comp(reg, "EAX") == 0) {
        return EAX;
    }
    else if(comp(reg, "EBX") == 0) {
        return EBX;
    }
    else if(comp(reg, "ECX") == 0) {
        return ECX;
    }
    else if(comp(reg, "EDX") == 0) {
        return EDX;
    }
    else if(comp(reg, "EEX") == 0) {
        return EEX;
    }
    else if(comp(reg, "EFX") == 0) {
        return EFX;
    }
    else if(comp(reg, "EGX") == 0) {
        return EGX;
    }
    else if(comp(reg, "EHX") == 0) {
        return EHX;
    }

    return UNAVAILABLE;
}

Reg BancoRegs::getRegEnum(const char* reg, bool& referencia ) {

    if( reg[0] != '[' ) //Não referencia
    {
        referencia = false;
        return this -> getRegEnum( reg );
    }
    else//referencia
    {
        reg += 1;//Anda um char
        referencia = true;
        if(comp(reg, "EAX]") == 0) {
            return EAX;
        }
        else if(comp(reg, "EBX]") == 0) {
            return EBX;
        }
        else if(comp(reg, "ECX]") == 0) {
            return ECX;
        }
        else if(comp(reg, "EDX]") == 0) {
            return EDX;
        }
        else if(comp(reg, "EEX]") == 0) {
            return EEX;
        }
        else if(comp(reg, "EFX]") == 0) {
            return EFX;
        }
        else if(comp(reg, "EGX]") == 0) {
            return EGX;
        }
        else if(comp(reg, "EHX]") == 0) {
            return EHX;
        }
    }

    return UNAVAILABLE;
}
