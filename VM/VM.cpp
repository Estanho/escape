#include "../includes/VM.h"
#include <unistd.h>
#include "../includes/TOOLS.h"

#define DEBUG_INSTRUCTIONS 0
#define DEBUG 0

VM::VM() {
	rodando = false;
    nProc = 0;
    atomic = 0;
    processoAtivo = NULL;
    escalonador = NULL;
    memoria = new Memoria( Janela::p_Instance()->get_tamRam(), Janela::p_Instance()->get_tamVir() , Janela::p_Instance()->get_pageSize() , &this->fileSystem );
    
    #if DEBUG
    cout << Janela::p_Instance()->get_tamRam() << " " << Janela::p_Instance()->get_tamVir() << " " << Janela::p_Instance()->get_pageSize() << endl ;
    #endif
    
	//memoria = new Memoria( 1024*1024 , 1024*1024, 4*1024,  &this->fileSystem  );// 1 Mega, 1 Mega e 4kb
}

bool VM::set_Escalonador(Escalonador* escalonador) {
	if(rodando) return false;

	if(this->escalonador) delete this->escalonador;

    this->escalonador = escalonador;
    this->escalonador->mapaProcesso = &mapaProcesso;
    this->escalonador->novosProcessos = new std::queue<PID>;

    return true;
}

VM::~VM() {
    delete escalonador;
    delete memoria;
}

Processo* VM::getProcesso(PID pid) {
    return (mapaProcesso.find(pid))->second;
}

Memoria * VM::get_Memoria()
{
    return memoria;
}


PID VM::criaProcesso(std::string INPUT) {
    Processo* processo;
    std::map<PID, Processo*>::iterator it;
    PID pid;

    //Processo

    nProc++;

    do {
        pid = (PID)rand()%(nProc*2) + 1; // > 0
        it = mapaProcesso.find(pid);
    } while(it != mapaProcesso.end()); //PID j� existente

    processo = new Processo(pid);

    if(!PARssembly_Reader::getPbuffer()->GetProc(INPUT, processo, memoria )) {
	  Janela::Print("Mem�ria insuficiente para inserir este processo!!");
	  return 0;
	  
    }

    mapaProcesso.insert(std::pair<PID, Processo*>(pid, processo));

    return pid;
}

PID VM::criaProcesso() { //DEPRECATED?
    Processo* processo;
    std::map<PID, Processo*>::iterator it;
    PID pid;

    nProc++;

    do {
        pid = (PID)rand()%(nProc*2) + 1; // > 0
        it = mapaProcesso.find(pid);
    } while(it != mapaProcesso.end()); //PID j� existente

    processo = new Processo(pid);

    mapaProcesso.insert(std::pair<PID, Processo*>(pid, processo));

    return pid;
}

void VM::registrar(nome_processo proc, PID pid) {
    mapaRegistro.insert(std::pair<nome_processo, PID>(proc, pid));
}

void VM::iniciar() {
    this->escalonador->Inicializar();

    while(1){
        processoAtivo = escalonador->escalonar();

        if(processoAtivo == NULL)
            break;

        cout << "Processo PID: " << processoAtivo->getPid() << endl;
        unsigned int quantum = escalonador->quantum;

        while(quantum--){

            if(atomic) { //Tem que pensar melhorzinho ainda
                quantum = 1; //Est� dentro de um bloco atomic
            }
            /*if(!stdOut.empty()){
                if((stdOut.front()).pid == 0)
                    std::cout << (stdOut.front()).caractere;
                else if(getProcesso((stdOut.front()).pid)->get_estado() == ESPERA && getProcesso((stdOut.front()).pid)->get_comRestantes() > 0)
                    getProcesso((stdOut.front()).pid)->set_estado(PRONTO); //ATEN��O: N�O SE USA MAIS COMRESTANTES
                else if(getProcesso((stdOut.front()).pid)->get_estado() == ESPERA)
                    getProcesso((stdOut.front()).pid)->set_estado(FINALIZADO);

                stdOut.pop();
            }*/
            escalonador->quantum = quantum;
            if(processoAtivo->get_estado() == ESPERA || processoAtivo->get_estado() == FINALIZADO) {
                escalonador->quantum = quantum - 1;
                break;
            }

            /** executa comando do processo */

            Comando comando = processoAtivo->pegaAtomico();
            switch(comando.COM) {
                case Comando::PRINT:
                #if DEBUG_INSTRUCTIONS
                    cout << "PRINT" << endl;
                #endif
                    cm_imprime(*(comando.args[0].str));
                    break;

                case Comando::PUSH:
                #if DEBUG_INSTRUCTIONS
                    cout << "PUSH" << endl;
                #endif
                    //cm_branchFun(comando.args[0].inteiro);
                    break;

                case Comando::POP:
                #if DEBUG_INSTRUCTIONS
                    cout << "POP" << endl;
                #endif
                    //cm_branchFun(comando.args[0].inteiro);
                    break;

                case Comando::CALL:
                #if DEBUG_INSTRUCTIONS
                    cout << "CALL" << endl;
                #endif
                    //cm_branchFun(comando.args[0].inteiro);
                    break;

                case Comando::RET:
                #if DEBUG_INSTRUCTIONS
                    cout << "RET" << endl;
                #endif
                    //cm_branchRet();
                    break;

                case Comando::JG:
                #if DEBUG_INSTRUCTIONS
                    cout << "JG" << endl;
                #endif
                    cm_jg(comando.args[0].inteiro);
                    break;

                case Comando::JGE:
                #if DEBUG_INSTRUCTIONS
                    cout << "JGE" << endl;
                #endif
                    cm_jge(comando.args[0].inteiro);
                    break;

                case Comando::JL:
                #if DEBUG_INSTRUCTIONS
                    cout << "JL" << endl;
                #endif
                    cm_jl(comando.args[0].inteiro);
                    break;

                case Comando::JLE:
                #if DEBUG_INSTRUCTIONS
                    cout << "JLE" << endl;
                #endif
                    cm_jle(comando.args[0].inteiro);
                    break;

                case Comando::JE:
                #if DEBUG_INSTRUCTIONS
                    cout << "JE" << endl;
                #endif
                    cm_je(comando.args[0].inteiro);
                    break;

                case Comando::JNE:
                #if DEBUG_INSTRUCTIONS
                    cout << "JNE" << endl;
                #endif
                    cm_jne(comando.args[0].inteiro);
                    break;

                case Comando::JA:
                #if DEBUG_INSTRUCTIONS
                    cout << "JA" << endl;
                #endif
                    //cm_branch(comando.args[0].inteiro);
                    break;

                case Comando::JAE:
                #if DEBUG_INSTRUCTIONS
                    cout << "JAE" << endl;
                #endif
                    //cm_branch(comando.args[0].inteiro);
                    break;

                case Comando::JB:
                #if DEBUG_INSTRUCTIONS
                    cout << "JB" << endl;
                #endif
                    //cm_branch(comando.args[0].inteiro);
                    break;

                case Comando::JBE:
                #if DEBUG_INSTRUCTIONS
                    cout << "JBE" << endl;
                #endif
                    //cm_branch(comando.args[0].inteiro);
                    break;

                case Comando::JMP:
                #if DEBUG_INSTRUCTIONS
                    cout << "JMP" << endl;
                #endif
                    cm_branch(comando.args[0].inteiro);
                    break;

                case Comando::LABEL:
                #if DEBUG_INSTRUCTIONS
                    cout << "LABEL" << endl;
                #endif
                    //cm_alloc_var(*(comando.args[0].str), comando.args[1].booleano);
                    break;

                case Comando::MOV:
                #if DEBUG_INSTRUCTIONS
                    cout << "MOV" << endl;
                #endif
                    cm_mov( *(comando.args[0].str) , *(comando.args[1].str) );
                    break;

                case Comando::MOVI:
                #if DEBUG_INSTRUCTIONS
                    cout << "MOVI" << endl;
                #endif
                    //cm_write_var(*(comando.args[0].str), comando.args[1].inteiro);
                    cm_movi( *(comando.args[0].str) , comando.args[1].imm );
                    break;

                case Comando::SET_PROC:
                #if DEBUG_INSTRUCTIONS
                    cout << "SET_PROC" << endl;
                #endif
                    //cm_set_proc(*(comando.args[0].comandos), *(comando.args[0].str), *(comando.args[0].str));
                    break;

                case Comando::START_ATOMIC:
                #if DEBUG_INSTRUCTIONS
                    cout << "START_ATOMIC" << endl;
                #endif
                    cm_start_atomic();
                    break;

                case Comando::END_ATOMIC:
                #if DEBUG_INSTRUCTIONS
                    cout << "END_ATOMIC" << endl;
                #endif
                    cm_end_atomic();
                    break;
                
                case Comando::START_PROC:
                #if DEBUG_INSTRUCTIONS
                    cout << "START_PROC" << endl;
                #endif
                    break;
                
                case Comando::END_PROC:
                #if DEBUG_INSTRUCTIONS
                    cout << "END_PROC" << endl;
                #endif
                    cm_end_proc();
                    break;
                
                case Comando::CMP:
                #if DEBUG_INSTRUCTIONS
                    cout << "CMP" << endl;
                #endif
                    cm_cmp(*(comando.args[0].str), *(comando.args[1].str));
                    break;

                case Comando::ADD:
                #if DEBUG_INSTRUCTIONS
                    cout << "ADD" << endl;
                #endif
                    cm_add(*(comando.args[0].str), *(comando.args[1].str));
                    break;

                case Comando::SUB:
                #if DEBUG_INSTRUCTIONS
                    cout << "SUB" << endl;
                #endif
                    //cm_sub(*(comando.args[0].str), *(comando.args[1].str));
                    break;

                case Comando::MUL:
                #if DEBUG_INSTRUCTIONS
                    cout << "MUL" << endl;
                #endif
                    cm_mul(*(comando.args[0].str));
                    break;

                case Comando::IMUL:
                #if DEBUG_INSTRUCTIONS
                    cout << "IMUL" << endl;
                #endif
                    //cm_mulc(*(comando.args[0].str), *(comando.args[1].str), comando.args[2].inteiro);
                    break;

                case Comando::DIV:
                #if DEBUG_INSTRUCTIONS
                    cout << "DIV" << endl;
                #endif
                    //cm_div(*(comando.args[0].str), *(comando.args[1].str));
                    break;

                case Comando::IDIV:
                #if DEBUG_INSTRUCTIONS
                    cout << "IDIV" << endl;
                #endif
                    //cm_divc(*(comando.args[0].str), *(comando.args[1].str), comando.args[2].inteiro);
                    break;

                case Comando::AND:
                #if DEBUG_INSTRUCTIONS
                    cout << "AND" << endl;
                #endif
                    //cm_and(*(comando.args[0].str), *(comando.args[1].str), *(comando.args[2].str));
                    break;

                case Comando::OR:
                #if DEBUG_INSTRUCTIONS
                    cout << "OR" << endl;
                #endif
                    //cm_or(*(comando.args[0].str), *(comando.args[1].str), *(comando.args[2].str));
                    break;

                case Comando::NOT:
                #if DEBUG_INSTRUCTIONS
                    cout << "NOT" << endl;
                #endif
                    //cm_not(*(comando.args[0].str), *(comando.args[1].str));
                    break;

                case Comando::XOR:
                #if DEBUG_INSTRUCTIONS
                    cout << "XOR" << endl;
                #endif
                    //cm_xor(*(comando.args[0].str), *(comando.args[1].str), *(comando.args[2].str));
                    break;
                default:
                    cout << "Erro: comando inexistente: " << comando.COM << endl;
                    break;
            }
        }
    }
}

/** COMANDOS */

void VM :: cm_mov( const string& arg1 , const string& arg2 )
{
    BancoRegs& banco = processoAtivo->getBancoRegs();
    bool reg1_ref, reg2_ref;
    Reg reg1 = banco.getRegEnum( arg1.c_str() ,reg1_ref );
    Reg reg2 = banco.getRegEnum( arg2.c_str() ,reg2_ref );
    unsigned memoryBegin = processoAtivo -> get_memoryBegin();
    
    

    if(reg2 == UNAVAILABLE) { //REG32 , imm32
        map<string, unsigned>::iterator it;

        it = processoAtivo->get_vars()->find(arg2);
        if( it == processoAtivo->get_vars()->end() )
        {
            throw Exception( __LINE__ , __FILE__,"Label invalido\n");
        }
        banco.getReg(reg1)->setVal(it->second);
    }
    else if( reg1_ref && reg2_ref ) //[REG32] , [REG32]
    {    
        unsigned lido = memoria -> access_4(  banco.getReg(reg2)->getVal() + memoryBegin  );
        memoria -> access_4 ( banco.getReg(reg1)->getVal() + memoryBegin, lido );
    }   
    else if ( reg1_ref )// [REG32] , REG32
    {
         memoria -> access_4 ( banco.getReg(reg1)->getVal()  + memoryBegin,  banco.getReg(reg2)->getVal()  );
    } 
    else if ( reg2_ref )// REG32 , [REG32]
    {
        unsigned lido = memoria -> access_4(  banco.getReg(reg2)->getVal() + memoryBegin );
        banco.getReg(reg1)->setVal( lido );
    }
    else 
    {
        banco.getReg(reg1)->setVal( banco.getReg(reg2)->getVal() );
    }

}
void VM :: cm_movi( const string& arg1 , const int arg2 ) {
	
    BancoRegs& banco = processoAtivo->getBancoRegs();
    
    Reg reg1 = banco.getRegEnum((char*)arg1.c_str());
    banco.getReg(reg1)->setVal(arg2);
}

void VM::cm_cmp(const string& arg1 , const string& arg2 ) {

    BancoRegs& banco = processoAtivo->getBancoRegs();

    Reg reg1 = banco.getRegEnum((char*)arg1.c_str());
    Reg reg2 = banco.getRegEnum((char*)arg2.c_str());

    bool* eq;
    bool* gr;
    bool* bl;

    eq = processoAtivo->check_eq();
    gr = processoAtivo->check_gr();
    bl = processoAtivo->check_bl();

    if(banco.getReg(reg1)->getVal() == banco.getReg(reg2)->getVal()) {
        *eq = true;
        *gr = false;
        *bl = false;
    }
    else if(banco.getReg(reg1)->getVal() > banco.getReg(reg2)->getVal()) {
        *eq = false;
        *gr = true;
        *bl = false;
    }
    else if(banco.getReg(reg1)->getVal() < banco.getReg(reg2)->getVal()) {
        *eq = false;
        *gr = false;
        *bl = true;
    }
}

void VM::cm_jg(int label) {
    bool gr = *processoAtivo->check_gr();
    if(gr)
        cm_branch(label);
}

void VM::cm_jge(int label) {
    bool gr = *processoAtivo->check_gr();
    bool eq = *processoAtivo->check_eq();
    if(gr || eq)
        cm_branch(label);
}

void VM::cm_jl(int label) {
    bool bl = *processoAtivo->check_bl();
    if(bl)
        cm_branch(label);
}

void VM::cm_jle(int label) {
    bool bl = *processoAtivo->check_bl();
    bool eq = *processoAtivo->check_eq();
    if(bl || eq)
        cm_branch(label);
}

void VM::cm_je(int label) {
    bool eq = *processoAtivo->check_gr();
    if(eq)
        cm_branch(label);
}

void VM::cm_jne(int label) {
    bool eq = *processoAtivo->check_eq();
    if(!eq)
        cm_branch(label);
}

void VM::subcm_imprime( string& str , unsigned adress )
{
    unsigned char c;
    unsigned lido;

    do
    {
        lido = memoria->access_4(adress);
        adress += 4;
        
        for( unsigned i = 0 ; i < 4 ; i++ )
        {
            c = * ( (unsigned char * ) &lido);
            #if DEBUG
            printf("Lido = %c | %d\n", c , lido);   
            #endif 
            
            if( c == '\0' )
            { 
                return;
            }

            str += c;
            lido >>= 8;
        }

    }
    while( true  );
}

void VM::cm_imprime( string str){
	BancoRegs& banco = processoAtivo->getBancoRegs();
	bool colchete;
    Reg reg1 = banco.getRegEnum( str.c_str() , colchete );

    unsigned adress;
    map<string, unsigned>::iterator it;	
    bool imp_string = false;

    if(reg1 == UNAVAILABLE) { //Lendo 
    
        if( colchete )
        {
            imp_string = true;
            removeColchete(str);
        }

        it = processoAtivo->get_vars()->find(str);
        if( it == processoAtivo->get_vars()->end() )
        {
            throw Exception( __LINE__ , __FILE__,"Label invalido\n");
        }
        adress = it-> second;
    }
    else
    {

        adress = banco.getReg(reg1)->getVal();
        if( colchete )
        {
            imp_string = true;    
        }
    }

    if( imp_string) //Imprimindo
    {
        #if DEBUG
        printf("Adress of string >>>> %u\n", adress);       
        #endif 

        adress += processoAtivo -> get_memoryBegin();
        #if DEBUG
        printf("\nMemoria begin : %d\n", processoAtivo ->  get_memoryBegin() );
        #endif

        string aux;
        this-> subcm_imprime( aux, adress);
        Janela::Print(aux);
    }
    else
    {
        char str_aux[20];
        sprintf(str_aux,"%u",adress);
        Janela::Print(str_aux);
    }
}

void VM::cm_imprime_var(variavel var) {
    int x = getvar(var)->var.inteiro;

    std::stringstream ss;
    ss << x;
    cm_imprime(ss.str());
}

void VM::cm_branchFun(int salto) {
    processoAtivo->setPC(salto);
}

void VM::cm_branchRet() {
    processoAtivo->PCPop();
    processoAtivo->popData();
    processoAtivo->setBranchF(processoAtivo->getBranchF() - 1);
}

void VM::cm_branchf(variavel var, int salto) {
    if(!(getvar(var)->var.inteiro)) {
        processoAtivo->setPC(processoAtivo->getPC() + salto);
    }
}

void VM::cm_brancht(variavel var, int salto) {
    if(getvar(var)->var.inteiro) {
        processoAtivo->setPC(processoAtivo->getPC() + salto);
    }
}

void VM::cm_branch(int salto) {
    processoAtivo->setPC(salto);
}

void VM::cm_alloc_var(variavel var, bool global) {
    if(global) {
        DATA newData;
        newData.pid = processoAtivo->getPid();
        newData.var.inteiro = 0;
        data.insert(std::pair<variavel, DATA>(var, newData));
    }
    else {
        processoAtivo->addVar(var);
    }
}

void VM::cm_write_var(variavel var, int val) {
    getvar(var)->var.inteiro = val;
}

void VM::cm_write_var(variavel var, variavel val) {
    getvar(var)->var.inteiro = getvar(val)->var.inteiro;
}


DATA* VM::getvar(variavel var) {
    DATA* retorno;
    std::map<variavel, DATA>::iterator it = (data.find(var));

    if( (retorno = processoAtivo->getvar(var)) ); //Se n�o for NULL
    else if(it != data.end())
        retorno = &(it->second);
    else
        exit(2);

    return retorno;
}

void VM::cm_set_proc(std::queue<Comando*> comandos, nome_processo nome, variavel var) {
    Processo* processo;
    PID pid;

    processo = getProcesso(pid = getvar(var)->var.inteiro = criaProcesso());
    registrar(nome, pid);

    while(!comandos.empty()) {
        processo->addComando(*(comandos.front()));
        comandos.pop();
    }

    this->escalonador->novosProcessos->push(processo->getPid());
}

void VM::cm_start_atomic() {
    atomic = 1;
}
void VM::cm_end_atomic() {
    atomic = 0;
}

void VM::cm_end_proc() {
    processoAtivo->set_estado(FINALIZADO);
    memoria -> deletePages( processoAtivo->get_pagina(),processoAtivo->get_numPaginas()  );
    Janela::p_Instance()->removeProc(processoAtivo->modulo);
}


void VM::cm_add(variavel a, variavel b) {
    BancoRegs& banco = processoAtivo->getBancoRegs();
    Reg reg1 = banco.getRegEnum((char*)a.c_str());
    Reg reg2 = banco.getRegEnum((char*)b.c_str());
    banco.getReg(reg1)->setVal(banco.getReg(reg1)->getVal() + banco.getReg(reg2)->getVal());
}
void VM::cm_sub(variavel a, variavel b) {
    BancoRegs& banco = processoAtivo->getBancoRegs();
    Reg reg1 = banco.getRegEnum((char*)a.c_str());
    Reg reg2 = banco.getRegEnum((char*)b.c_str());
    banco.getReg(reg1)->setVal(banco.getReg(reg1)->getVal() - banco.getReg(reg2)->getVal());
}

void VM::cm_mul(variavel a) {
    BancoRegs& banco = processoAtivo->getBancoRegs();
    Reg reg1 = banco.getRegEnum((char*)a.c_str());
    Reg eax = banco.getRegEnum("EAX");
   
    banco.getReg(reg1)->setVal(banco.getReg(reg1)->getVal() * banco.getReg(eax)->getVal());
}

void VM::cm_mulc(variavel a, variavel b, int c) {
    DATA* A = getvar(a);
    DATA* B = getvar(b);

    A->var.inteiro = B->var.inteiro * c;
}

void VM::cm_div(variavel a, variavel b) {
    BancoRegs& banco = processoAtivo->getBancoRegs();
    Reg reg1 = banco.getRegEnum((char*)a.c_str());
    Reg reg2 = banco.getRegEnum((char*)b.c_str());
    if(banco.getReg(reg2)->getVal() == 0) throw Exception( __LINE__ , __FILE__ , "Divis�o por 0." );
    banco.getReg(reg1)->setVal(banco.getReg(reg1)->getVal() / banco.getReg(reg2)->getVal());
}

void VM::cm_cdiv(variavel a, variavel b, int c) {
    DATA* A = getvar(a);
    DATA* B = getvar(b);

    A->var.inteiro = B->var.inteiro / c;
}

void VM::cm_divc(variavel a, variavel b, int c) {
    DATA* A = getvar(a);
    DATA* B = getvar(b);

    A->var.inteiro = c / B->var.inteiro;
}

void VM::cm_and(variavel a, variavel b, variavel c) {
    DATA* A = getvar(a);
    DATA* B = getvar(b);
    DATA* C = getvar(c);

    A->var.inteiro = B->var.inteiro & C->var.inteiro;
}

void VM::cm_andc(variavel a, variavel b, int c) {
    DATA* A = getvar(a);
    DATA* B = getvar(b);

    A->var.inteiro = B->var.inteiro & c;
}

void VM::cm_or(variavel a, variavel b, variavel c) {
    DATA* A = getvar(a);
    DATA* B = getvar(b);
    DATA* C = getvar(c);

    A->var.inteiro = B->var.inteiro | C->var.inteiro;
}

void VM::cm_orc(variavel a, variavel b, int c) {
    DATA* A = getvar(a);
    DATA* B = getvar(b);

    A->var.inteiro = B->var.inteiro | c;
}

void VM::cm_not(variavel a, variavel b) {
    DATA* A = getvar(a);
    DATA* B = getvar(b);

    A->var.inteiro = !B->var.inteiro;
}

void VM::cm_notc(variavel a, int c) {
    DATA* A = getvar(a);

    A->var.inteiro = !c;
}

void VM::cm_xor(variavel a, variavel b, variavel c) {
    DATA* A = getvar(a);
    DATA* B = getvar(b);
    DATA* C = getvar(c);

    A->var.inteiro = B->var.inteiro ^ C->var.inteiro;
}

void VM::cm_xorc(variavel a, variavel b, int c) {
    DATA* A = getvar(a);
    DATA* B = getvar(b);

    A->var.inteiro = B->var.inteiro ^ c;
}

void VM::cm_andl(variavel a, variavel b, variavel c) {
    DATA* A = getvar(a);
    DATA* B = getvar(b);
    DATA* C = getvar(c);

    A->var.inteiro = B->var.inteiro && C->var.inteiro;
}

void VM::cm_andlc(variavel a, variavel b, int c) {
    DATA* A = getvar(a);
    DATA* B = getvar(b);

    A->var.inteiro = B->var.inteiro && c;
}

void VM::cm_orl(variavel a, variavel b, variavel c) {
    DATA* A = getvar(a);
    DATA* B = getvar(b);
    DATA* C = getvar(c);

    A->var.inteiro = B->var.inteiro || C->var.inteiro;
}

void VM::cm_orlc(variavel a, variavel b, int c) {
    DATA* A = getvar(a);
    DATA* B = getvar(b);

    A->var.inteiro = B->var.inteiro || c;
}

void VM::cm_notl(variavel a, variavel b) {
    DATA* A = getvar(a);
    DATA* B = getvar(b);

    A->var.inteiro = 1 && !B->var.inteiro;
}

void VM::cm_notlc(variavel a, int c) {
    DATA* A = getvar(a);

    A->var.inteiro = 1 && !c;
}

void VM::cm_xorl(variavel a, variavel b, variavel c) {
    DATA* A = getvar(a);
    DATA* B = getvar(b);
    DATA* C = getvar(c);

    A->var.inteiro = !B->var.inteiro != !C->var.inteiro;
}

void VM::cm_xorlc(variavel a, variavel b, int c) {
    DATA* A = getvar(a);
    DATA* B = getvar(b);

    A->var.inteiro = !B->var.inteiro != !c;
}

void VM::cm_gt(variavel a, variavel b, variavel c) {
    DATA* A = getvar(a);
    DATA* B = getvar(b);
    DATA* C = getvar(c);

    A->var.inteiro = B->var.inteiro > C->var.inteiro;
}
void VM::cm_lt(variavel a, variavel b, variavel c) {
    DATA* A = getvar(a);
    DATA* B = getvar(b);
    DATA* C = getvar(c);

    A->var.inteiro = B->var.inteiro < C->var.inteiro;
}
void VM::cm_eq(variavel a, variavel b, variavel c) {
    DATA* A = getvar(a);
    DATA* B = getvar(b);
    DATA* C = getvar(c);

    A->var.inteiro = B->var.inteiro == C->var.inteiro;
}
void VM::cm_neq(variavel a, variavel b, variavel c) {
    DATA* A = getvar(a);
    DATA* B = getvar(b);
    DATA* C = getvar(c);

    A->var.inteiro = B->var.inteiro != C->var.inteiro;
}
void VM::cm_ge(variavel a, variavel b, variavel c) {
    DATA* A = getvar(a);
    DATA* B = getvar(b);
    DATA* C = getvar(c);

    A->var.inteiro = B->var.inteiro >= C->var.inteiro;
}
void VM::cm_le(variavel a, variavel b, variavel c) {
    DATA* A = getvar(a);
    DATA* B = getvar(b);
    DATA* C = getvar(c);

    A->var.inteiro = B->var.inteiro <= C->var.inteiro;
}
void VM::cm_eqc(variavel a, variavel b, int c) {
    DATA* A = getvar(a);
    DATA* B = getvar(b);

    A->var.inteiro = B->var.inteiro == c;
}
void VM::cm_gtc(variavel a, variavel b, int c) {
    DATA* A = getvar(a);
    DATA* B = getvar(b);

    A->var.inteiro = B->var.inteiro > c;
}
void VM::cm_ltc(variavel a, variavel b, int c) {
    DATA* A = getvar(a);
    DATA* B = getvar(b);

    A->var.inteiro = B->var.inteiro < c;
}
void VM::cm_neqc(variavel a, variavel b, int c) {
    DATA* A = getvar(a);
    DATA* B = getvar(b);

    A->var.inteiro = B->var.inteiro != c;
}
void VM::cm_gec(variavel a, variavel b, int c) {
    DATA* A = getvar(a);
    DATA* B = getvar(b);

    A->var.inteiro = B->var.inteiro >= c;
}
void VM::cm_lec(variavel a, variavel b, int c) {
    DATA* A = getvar(a);
    DATA* B = getvar(b);

    A->var.inteiro = B->var.inteiro <= c;
}

void Janela::Init_Process_Window(MainWindow* w) {
    ProcessWindow = w;
    
    pthread_t stdout_thread;
    pthread_create(&stdout_thread, NULL, _escape_stdout, NULL);
}

void Janela::Init_Memory_Window(MainWindow* w) {
    MemoryWindow = w;
}

void *Janela::_escape_stdout(void* t) {
	while(1) { //ENQUANTO N�O RECEBER SINGAL()

		__debufferize__();
		
	}
    return NULL;
}

void Janela::_print(std::string str) {
	std::string strptr = str;
	
	p_Instance()->set_Terminal_line(strptr);
	
	//delete strptr;
	
	pthread_mutex_unlock(&OUTPUT);
}

void Janela::__debufferize__() {
	if(!pthread_mutex_trylock(&OUTPUT)) {
		if(strptrs.size() > 0) {
		
			std::string strptr = strptrs.front();
			strptrs.pop();

			_print(strptr);
            usleep(30000);
		}
		else {
			pthread_mutex_unlock(&OUTPUT);
		}
	}
}

void Janela::Print(std::string str) {
    //str.append("\n");

	//std::string* strptr = new string(str);

    while(strptrs.size() > 100) {
        usleep(50000);
    }


	strptrs.push(str);
	
	//pthread_exit(NULL);
}
