#ifndef QT_GUI_HANDLER_H
#define QT_GUI_HANDLER_H

#include <string>
#include "memwindow.h"

class QT_GUI_HANDLER
{
public:
    QT_GUI_HANDLER();

    static void (*CBRunFunction)();
    static bool (*CBInsertFunction)(std::string);
    static bool (*CBReadFunction)(std::string);
    static void (*CBUnReadFunction)(std::string);
    static void (*CBMountVm)();

    static MemWindow* mui;

    static void setCenterOfApplication(QWidget *w, bool useSizeHint);

    static long int tamRam;
    static long int tamVir;
    static long int tamPaginas;
};

#endif // QT_GUI_HANDLER_H
