/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created: Fri Aug 23 16:34:18 2013
**      by: Qt User Interface Compiler version 4.8.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QComboBox>
#include <QtGui/QFrame>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QListWidget>
#include <QtGui/QMainWindow>
#include <QtGui/QMenu>
#include <QtGui/QMenuBar>
#include <QtGui/QPushButton>
#include <QtGui/QStatusBar>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QAction *actionRodar;
    QAction *actionAutoScroll;
    QAction *actionSair;
    QWidget *centralwidget;
    QFrame *frame;
    QListWidget *Terminal;
    QFrame *frame_2;
    QListWidget *runList;
    QLabel *listLabel_2;
    QListWidget *procList;
    QPushButton *addProc;
    QPushButton *remProc;
    QLabel *listLabel;
    QLabel *numProcs;
    QLabel *label;
    QComboBox *algoritmo;
    QPushButton *Memoria;
    QPushButton *Liberar;
    QPushButton *MontarVm;
    QMenuBar *menubar;
    QMenu *menuOp_es;
    QStatusBar *statusbar;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QString::fromUtf8("MainWindow"));
        MainWindow->resize(1024, 605);
        MainWindow->setMinimumSize(QSize(1024, 605));
        MainWindow->setMaximumSize(QSize(1024, 605));
        actionRodar = new QAction(MainWindow);
        actionRodar->setObjectName(QString::fromUtf8("actionRodar"));
        actionAutoScroll = new QAction(MainWindow);
        actionAutoScroll->setObjectName(QString::fromUtf8("actionAutoScroll"));
        actionAutoScroll->setCheckable(true);
        actionAutoScroll->setChecked(true);
        actionSair = new QAction(MainWindow);
        actionSair->setObjectName(QString::fromUtf8("actionSair"));
        centralwidget = new QWidget(MainWindow);
        centralwidget->setObjectName(QString::fromUtf8("centralwidget"));
        frame = new QFrame(centralwidget);
        frame->setObjectName(QString::fromUtf8("frame"));
        frame->setGeometry(QRect(10, 50, 751, 491));
        frame->setFrameShape(QFrame::StyledPanel);
        frame->setFrameShadow(QFrame::Raised);
        Terminal = new QListWidget(frame);
        Terminal->setObjectName(QString::fromUtf8("Terminal"));
        Terminal->setGeometry(QRect(10, 10, 731, 471));
        frame_2 = new QFrame(centralwidget);
        frame_2->setObjectName(QString::fromUtf8("frame_2"));
        frame_2->setGeometry(QRect(770, 50, 251, 491));
        frame_2->setFrameShape(QFrame::StyledPanel);
        frame_2->setFrameShadow(QFrame::Raised);
        runList = new QListWidget(frame_2);
        runList->setObjectName(QString::fromUtf8("runList"));
        runList->setGeometry(QRect(10, 290, 231, 191));
        runList->setResizeMode(QListView::Adjust);
        listLabel_2 = new QLabel(frame_2);
        listLabel_2->setObjectName(QString::fromUtf8("listLabel_2"));
        listLabel_2->setGeometry(QRect(10, 270, 201, 21));
        procList = new QListWidget(frame_2);
        procList->setObjectName(QString::fromUtf8("procList"));
        procList->setGeometry(QRect(10, 70, 231, 191));
        procList->setResizeMode(QListView::Adjust);
        addProc = new QPushButton(frame_2);
        addProc->setObjectName(QString::fromUtf8("addProc"));
        addProc->setGeometry(QRect(10, 20, 131, 31));
        remProc = new QPushButton(frame_2);
        remProc->setObjectName(QString::fromUtf8("remProc"));
        remProc->setGeometry(QRect(150, 20, 95, 31));
        listLabel = new QLabel(frame_2);
        listLabel->setObjectName(QString::fromUtf8("listLabel"));
        listLabel->setGeometry(QRect(10, 50, 201, 21));
        numProcs = new QLabel(frame_2);
        numProcs->setObjectName(QString::fromUtf8("numProcs"));
        numProcs->setGeometry(QRect(220, 270, 41, 21));
        label = new QLabel(centralwidget);
        label->setObjectName(QString::fromUtf8("label"));
        label->setGeometry(QRect(10, 20, 65, 21));
        algoritmo = new QComboBox(centralwidget);
        algoritmo->setObjectName(QString::fromUtf8("algoritmo"));
        algoritmo->setGeometry(QRect(770, 10, 251, 31));
        Memoria = new QPushButton(centralwidget);
        Memoria->setObjectName(QString::fromUtf8("Memoria"));
        Memoria->setGeometry(QRect(660, 10, 101, 31));
        Liberar = new QPushButton(centralwidget);
        Liberar->setObjectName(QString::fromUtf8("Liberar"));
        Liberar->setGeometry(QRect(70, 20, 51, 20));
        MontarVm = new QPushButton(centralwidget);
        MontarVm->setObjectName(QString::fromUtf8("MontarVm"));
        MontarVm->setGeometry(QRect(560, 10, 95, 31));
        MainWindow->setCentralWidget(centralwidget);
        menubar = new QMenuBar(MainWindow);
        menubar->setObjectName(QString::fromUtf8("menubar"));
        menubar->setGeometry(QRect(0, 0, 1024, 29));
        menuOp_es = new QMenu(menubar);
        menuOp_es->setObjectName(QString::fromUtf8("menuOp_es"));
        MainWindow->setMenuBar(menubar);
        statusbar = new QStatusBar(MainWindow);
        statusbar->setObjectName(QString::fromUtf8("statusbar"));
        MainWindow->setStatusBar(statusbar);

        menubar->addAction(menuOp_es->menuAction());
        menuOp_es->addAction(actionRodar);
        menuOp_es->addAction(actionAutoScroll);
        menuOp_es->addSeparator();
        menuOp_es->addAction(actionSair);

        retranslateUi(MainWindow);
        QObject::connect(Liberar, SIGNAL(clicked()), Terminal, SLOT(clear()));

        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "ESCAPE", 0, QApplication::UnicodeUTF8));
        actionRodar->setText(QApplication::translate("MainWindow", "Rodar!", 0, QApplication::UnicodeUTF8));
        actionAutoScroll->setText(QApplication::translate("MainWindow", "AutoScroll", 0, QApplication::UnicodeUTF8));
        actionSair->setText(QApplication::translate("MainWindow", "Sair", 0, QApplication::UnicodeUTF8));
        listLabel_2->setText(QApplication::translate("MainWindow", "Lista de Processos para Rodar", 0, QApplication::UnicodeUTF8));
        addProc->setText(QApplication::translate("MainWindow", "Adicionar Processo", 0, QApplication::UnicodeUTF8));
        remProc->setText(QApplication::translate("MainWindow", "Remover", 0, QApplication::UnicodeUTF8));
        listLabel->setText(QApplication::translate("MainWindow", "Lista de Processos Dispon\303\255veis", 0, QApplication::UnicodeUTF8));
        numProcs->setText(QApplication::translate("MainWindow", "0", 0, QApplication::UnicodeUTF8));
        label->setText(QApplication::translate("MainWindow", "Terminal", 0, QApplication::UnicodeUTF8));
        algoritmo->clear();
        algoritmo->insertItems(0, QStringList()
         << QApplication::translate("MainWindow", "Escolha o Algoritmo", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("MainWindow", "FIFO", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("MainWindow", "SJF", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("MainWindow", "Round-Robin", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("MainWindow", "MFQ", 0, QApplication::UnicodeUTF8)
        );
        Memoria->setText(QApplication::translate("MainWindow", "Mem\303\263ria", 0, QApplication::UnicodeUTF8));
        Liberar->setText(QApplication::translate("MainWindow", "Clear", 0, QApplication::UnicodeUTF8));
        MontarVm->setText(QApplication::translate("MainWindow", "Montar VM!", 0, QApplication::UnicodeUTF8));
        menuOp_es->setTitle(QApplication::translate("MainWindow", "Op\303\247\303\265es", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
