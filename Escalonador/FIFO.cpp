#include "FIFO.h"

void FIFO::Inicializar() {
    for(std::map<PID, Processo*>::const_iterator it = get_mapaProcesso()->begin(); it != get_mapaProcesso()->end(); it++)
        processos.push(it->second);
}

Processo* FIFO::escalonar() {
    set_quantum(1 << 31 /*Um número bem grande*/);

    if(processos.front()->get_estado() == FINALIZADO) processos.pop();
    if(processos.empty()) return NULL;

    return processos.front();
};
