#include "../includes/Processo.h"
#include <stdio.h>


unsigned Processo::get_numPaginas()
{
    return numPaginas;
}
unsigned Processo::get_pagina()
{
    return pagina;
}   

void Processo::set_numPaginas( unsigned numPaginas) 
{
    this -> numPaginas = numPaginas;
}

void Processo::set_vars(std::map<variavel, unsigned>* vars)
{
    this->vars = vars;
}

void Processo::set_memoryBegin( unsigned memoryBegin)
{
    this -> memoryBegin = memoryBegin; 
}

void Processo::set_pagina(unsigned pagina )
{
    this -> pagina = pagina;
}

Processo::Processo(PID pid) {
    _PID = pid;
    branchFun = 0;
    estado = PRONTO; //Por padr�o?

    data.push(new std::map<variavel, DATA>); //Escopo da fun��o principal

    Comando start_proc;
    start_proc.args = NULL;
    start_proc.COM = Comando::START_PROC;
    _ComList.push_back(start_proc);
    PC.push(0);
}

void Processo::addComando(Comando comando) {
    _ComList.insert(_ComList.end(), comando);
}

void Processo::addVar(variavel var) {
    DATA newData;
    newData.pid = _PID;
    newData.var.inteiro = 0;
    data.top()->insert(std::pair<variavel, DATA>(var, newData)); //Adiciona vari�vel nova ao escopo
}

DATA* Processo::getvar(variavel var) {
    std::map<variavel, DATA>::iterator it = data.top()->find(var);
    if(it == data.top()->end()) {
        return NULL;
    }
    else {
        return &(it->second);
    }
}

Comando Processo::pegaAtomico() {

    if(estado == FINALIZADO) {
        exit(1);
    }
    Comando retorno = _ComList[PC.top()];

    PC.top()++;

    return retorno;
}

void Processo::pushData() {
    data.push(new std::map<variavel, DATA>);
}
void Processo::popData() {
    data.pop();
}

void Processo::setPC(int newPC) {
    PC.top() = newPC;
}
int Processo::getPC() {
    return PC.top();
}
void Processo::PCPush(int newPC) {
    PC.push(newPC);
}
void Processo::PCPop() {
    PC.pop();
}

void Processo::setBranchF(int set) {
    branchFun = set;
}
int Processo::getBranchF() {
    return branchFun;
}

unsigned int Processo::get_comRestantes() const {
    return _ComList.size();
}

unsigned int Processo::get_comTotais() const {
    return _ComList.size();
}

PID Processo::getPid() {
    return _PID;
}

void Processo::set_estado(Estado estado) {
    this->estado = estado;
}

Estado Processo::get_estado() {
    return estado;
}
