#include "DumbFifo.h"
#define DEBUG 0

#if DEBUG
#include <cstdio>
#endif 

DumbFifo::DumbFifo( Memoria * memoria ) : MemoriaVirtual( memoria ) 
{
	if( tamMemReal != tamMemVirtual )
	{
		throw Exception( __LINE__ , __FILE__ , "O algortimo DumbFifo nao suporta MemoriaFisica diferente de MemoriaVirtual!!" );
	}
	num_paginas = tamMemReal / tamPagina;
}


unsigned DumbFifo::access_4(unsigned address){
	return memFisica->access_4(address);
}
void DumbFifo::access_4(unsigned address, unsigned value){
	memFisica->access_4(address,value);
}

bool DumbFifo::newPages(unsigned& paginaVirtual,unsigned numeroPaginas){
	static unsigned counter = 0;
	paginaVirtual = counter;
	counter += numeroPaginas;

	if( counter >= num_paginas ) 
		return false; ///Não tem mais memoria. OBS : DumbFifo fica sem memoria muito rápido, porque nunca desaloca nada que já foi alocado.
	
	#if DEBUG
		printf("newPages(%d,%d) = %d\n",paginaVirtual, numeroPaginas, true);
	#endif
	return true;
}

void DumbFifo::deletePages(unsigned paginaVirtual, unsigned numeroPaginas){
	return;
}

bool DumbFifo::statusPage(unsigned& paginaFisica, unsigned paginaVirtual){
	return true;
}


#undef DEBUG

