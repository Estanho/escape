#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QFileDialog>
#include <QListWidgetItem>
#include <QTextCursor>

#include "memwindow.h"

#define ALG_INVALID 0
#define ALG_FIFO 1
#define ALG_SJF 2
#define ALG_ROUND 3
#define ALG_MFQ 4

#define MAX_LINES 130

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:

    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

    void set_Read(bool (r_f)(std::string));
    void set_UnRead(void (u_f)(std::string));
    void set_Insert(bool (i_f)(std::string));
    void set_Run(void (r_f)());
    void set_MountVm(void (r_vm)());

    void removeProc(const char* item);

    int get_Algorithm();

    void inc_Procs();

    void dec_Procs();

    bool set_Terminal_line(std::string);

    int get_tamRam();
    int get_tamVir();
    int get_pageSize();

private slots:
    void on_addProc_clicked();

    void on_procList_doubleClicked(const QModelIndex &index);

    void on_Terminal_textChanged();

    void on_remProc_clicked();

    void on_actionSair_triggered();

    void on_actionRodar_triggered();

    void on_Memoria_clicked();

    void on_MontarVm_clicked();

    void on_Liberar_clicked();

    void on_Terminal_itemEntered(QListWidgetItem *);

private:
    Ui::MainWindow *ui;

    void exit();

};

#endif // MAINWINDOW_H
