#ifndef MEMWINDOW_H
#define MEMWINDOW_H

#include <QMainWindow>

#include "opcoes.h"

namespace Ui {
class MemWindow;
}

class MemWindow : public QMainWindow
{
    Q_OBJECT
    
public:
    explicit MemWindow(QWidget *parent = 0);
    ~MemWindow();

    inline int get_pageSize() {
        if(!pui) return PADRAO_PAGINA;
        return pui->get_pageSize();
    }

    inline int get_tamRam() {
        if(!pui) return PADRAO_REAL;
        return pui->get_tamRam();
    }
    inline int get_tamVir() {
        if(!pui) return PADRAO_VIRTUAL;
        return pui->get_tamVir();
    }

    void InsertVirtualLine(QString info); //Coluna separada por espaço!
    void InsertRealLine(QString info); //Coluna separada por espaço!
    void InsertTabelaLine(QString info); //Coluna separada por espaço!


    void EditVirtualLine(int row, QString info); //Coluna separada por espaço!
    void EditRealLine(int row, QString info); //Coluna separada por espaço!
    void EditTabelaLine(int row, QString info); //Coluna separada por espaço!


private slots:

    void on_Opcoes_clicked();

    void on_actionFechar_triggered();

private:
    Ui::MemWindow *ui;

    Opcoes* pui;
};

#endif // MEMWINDOW_H
